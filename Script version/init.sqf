//A3 HAC config
RHQ_Recon = [];  

RHQ_FO = [];

RHQ_Snipers = [
"B_soldier_M_F",
"O_soldier_M_F"];

RHQ_ATInf = [
"B_soldier_LAT_F",
"O_Soldier_LAT_F"];

RHQ_AAInf = []; 

RHQ_Inf = [
"B_medic_F",
"B_soldier_AR_F",
"B_soldier_exp_F",
"B_Soldier_F",
"B_Soldier_GL_F",
"B_Soldier_lite_F",
"B_Soldier_SL_F",
"B_Soldier_TL_F",
"O_medic_F",
"O_Soldier_AR_F",
"O_soldier_exp_F",
"O_Soldier_F",
"O_Soldier_GL_F",
"O_Soldier_lite_F",
"O_Soldier_SL_F",
"O_Soldier_TL_F"];

RHQ_HArmor = [];

RHQ_MArmor = [];

RHQ_LArmor = [];  

RHQ_LarmorAT = [];  

RHQ_Cars = [
"B_MRAP_01_F",
"B_MRAP_01_gmg_F",
"B_MRAP_01_hmg_F",
"O_MRAP_02_F",
"O_MRAP_02_gmg_F",
"O_MRAP_02_hmg_F",
"C_Offroad_01_F",
"B_Quadbike_01_F",
"O_Quadbike_01_F"];  

RHQ_Air = [
"B_Heli_Light_01_armed_F",
"O_Heli_Light_02_F",
"O_Heli_Light_02_unarmed_F",
"B_Heli_Light_01_F"];

RHQ_NCAir = [
"O_Heli_Light_02_unarmed_F",
"B_Heli_Light_01_F"];

RHQ_BAir = [];
RHQ_RAir = [];
RHQ_Naval = [
"B_Boat_Transport_01_F",
"O_Boat_Transport_01_F",
"O_Boat_Armed_01_hmg_F",
"B_Boat_Armed_01_minigun_F"];

RHQ_Static = [];

RHQ_StaticAA = [];

RHQ_StaticAT = [];

RHQ_Support = [];

RHQ_Med = [];

RHQ_Ammo = [];

RHQ_Fuel = [];

RHQ_Rep = [];

RHQ_Cargo = [
"B_MRAP_01_hmg_F",
"B_MRAP_01_gmg_F",
"O_MRAP_02_gmg_F",
"O_MRAP_02_hmg_F",
"B_MRAP_01_F",
"O_MRAP_02_F",
"O_Heli_Light_02_F",
"O_Heli_Light_02_unarmed_F",
"B_Heli_Light_01_F"]; 

RHQ_NCCargo = [
"O_Heli_Light_02_unarmed_F",
"B_Heli_Light_01_F",
"B_MRAP_01_F",
"O_MRAP_02_F"];  

RHQ_Crew = [
"B_Helipilot_F",
"O_helipilot_F"];
//Init HAC
nul = [] execVM "RydHQInit.sqf";