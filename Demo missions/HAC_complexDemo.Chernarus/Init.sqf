//Debug for all

RydHQ_Debug = true;
RydHQB_Debug = true;
RydHQC_Debug = true;

//Surrendering turned off for all (not compatibile with more than two commanders yet)

RydHQ_Surr = false;
RydHQB_Surr = false;
RydHQC_Surr = false;

//Manual personality setting on for A i C 

RydHQ_MAtt = true;
RydHQC_MAtt = true;

//Choosing of personalities for A i C

RydHQ_Personality = "GENIUS";
RydHQC_Personality = "GENIUS";

//Cargo system for all

RydHQ_CargoFind = 100;
RydHQB_CargoFind = 100;
RydHQC_CargoFind = 100;

//Fronts for A and B

RydHQ_Front = true;
RydHQB_Front = true;

//Unlimited control off for A and B

RydHQ_SubAll = false;
RydHQB_SubAll = false;

//"Synchronized only" limited control mode on for B, used for adding under leaderHQB control some guerilla groups

RydHQB_SubSynchro = true;

//Six garrisoned groups for C (names set in init fields of teamleaders)

RydHQC_Garrison = [g1,g2,g3,g4,g5,g6];

//Quick method for adding under leader's control all groups of given faction present on map at start, for A i B

RydHQ_Included = [];
RydHQB_Included = [];

	{
	if ((faction (leader _x)) == "USMC") then {RydHQ_Included = RydHQ_Included + [_x]};
	if ((faction (leader _x)) == "CDF") then {RydHQB_Included = RydHQB_Included + [_x]};
	}
foreach AllGroups;

//making some A-side groups nearly passive may be good for groups, that should all the time to concentrate on idle missions (?)

RydHQ_AOnly = [g7,g8];
RydHQ_ROnly = [g7,g8];
RydHQ_NoFlank = [g7,g8];
RydHQ_NoDef = [g7,g8];

//Player's unit excluded

RydHQ_ExcludedG = [P1];

//This only for script version:

//nul = [] execVM "RydHQInit.sqf";

//Let's play! :)