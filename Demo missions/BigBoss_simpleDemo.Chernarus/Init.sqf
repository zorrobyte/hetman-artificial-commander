//markers

RydHQ_Debug = true;

RydHQ_DebugII = true;

//limited control

RydHQ_SubAll = false;

RydHQ_SubSynchro = true;

RydHQ_Surr = true;

//to make things faster

RydHQ_NoRec = 10000;

RydHQ_RapidCapt = 10000;

RydHQ_Rush = true;

//BB setting

RydBBa_HQs = [leaderHQ];

RydBB_Active = true;
RydBB_Debug = true;
