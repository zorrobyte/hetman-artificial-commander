RHQ_SpecFor_OA = 
	[
	"US_Delta_Force_AR_EP1",
	"US_Delta_Force_MG_EP1",
	"US_Delta_Force_Marksman_EP1",
	"US_Delta_Force_Medic_EP1",
	"US_Delta_Force_EP1",
	"US_Delta_Force_Assault_EP1",
	"US_Delta_Force_M14_EP1",
	"US_Delta_Force_Night_EP1",
	"US_Delta_Force_SD_EP1",
	"US_Delta_Force_TL_EP1",
	"US_Delta_Force_Air_Controller_EP1",
	"TK_Special_Forces_MG_EP1",
	"TK_Special_Forces_EP1",
	"TK_Special_Forces_TL_EP1",
	"CZ_Special_Forces_DES_EP1",
	"CZ_Special_Forces_GL_DES_EP1",
	"CZ_Special_Forces_MG_DES_EP1",
	"CZ_Special_Forces_Scout_DES_EP1",
	"CZ_Special_Forces_TL_DES_EP1"
	];

RHQ_SpecFor_ACR = 
	[
	"CZ_Soldier_Spec_Demo_Wdl_ACR",
	"CZ_Soldier_Spec3_Wdl_ACR",
	"CZ_Soldier_Spec2_Wdl_ACR",
	"CZ_Soldier_Spec1_Wdl_ACR"
	];

RHQ_SpecFor_BAF = 
	[
	];

RHQ_SpecFor_PMC = 
	[
	];

RHQ_Recon_OA = 
	[
	"GER_Soldier_Scout_EP1",
	"US_Soldier_Spotter_EP1",
	"TK_Soldier_Spotter_EP1"
	];

RHQ_Recon_ACR = 
	[
	"CZ_Soldier_Spotter_ACR",
	"CZ_Soldier_Recon_Wdl_ACR"
	];

RHQ_Recon_BAF = 
	[
	"BAF_Soldier_spotter_W",
	"BAF_Soldier_spotterN_W"
	];

RHQ_Recon_PMC = 
	[
	];

RHQ_FO_OA = 
	[
	"GER_Soldier_Scout_EP1",
	"US_Soldier_Spotter_EP1",
	"TK_Soldier_Spotter_EP1"
	];

RHQ_FO_ACR = 
	[
	"CZ_Soldier_Spotter_ACR"
	];

RHQ_FO_BAF = 
	[
	"BAF_Soldier_spotter_W",
	"BAF_Soldier_spotterN_W"
	];

RHQ_FO_PMC = 
	[
	];

RHQ_Snipers_OA = 
	[
	"CZ_Soldier_Sniper_EP1",
	"US_Soldier_Marksman_EP1",
	"TK_Soldier_Sniper_EP1",
	"TK_Soldier_SniperH_EP1",
	"TK_Soldier_Sniper_Night_EP1",
	"TK_INS_Soldier_Sniper_EP1",
	"TK_GUE_Soldier_Sniper_EP1",
	"US_Soldier_Sniper_EP1",
	"US_Soldier_SniperH_EP1",
	"US_Soldier_Sniper_NV_EP1"
	];

RHQ_Snipers_ACR = 
	[
	"CZ_Soldier_Sniper_ACR",
	"CZ_Sharpshooter_Wdl_ACR",
	"CZ_Sharpshooter_DES_ACR"
	];

RHQ_Snipers_BAF = 
	[
	"BAF_Soldier_SniperN_MTP",
	"BAF_Soldier_SniperH_MTP",
	"BAF_Soldier_Sniper_MTP",
	"BAF_Soldier_SniperN_W",
	"BAF_Soldier_SniperH_W",
	"BAF_Soldier_Sniper_W",
	"BAF_Soldier_Marksman_DDPM"
	];

RHQ_Snipers_PMC = 
	[
	"Soldier_Sniper_PMC",
	"Soldier_Sniper_KSVK_PMC"
	];

RHQ_ATInf_OA = 
	[
	"TK_GUE_Soldier_HAT_EP1",
	"TK_GUE_Soldier_AT_EP1",
	"US_Soldier_AT_EP1",
	"US_Soldier_HAT_EP1",
	"US_Soldier_LAT_EP1",
	"TK_Soldier_HAT_EP1",
	"TK_Soldier_LAT_EP1",
	"TK_Soldier_AT_EP1",
	"TK_INS_Soldier_AT_EP1",
	"UN_CDF_Soldier_AT_EP1"
	];

RHQ_ATInf_ACR = 
	[
	"CZ_Soldier_RPG_Wdl_ACR",
	"CZ_Soldier_AT_Wdl_ACR"
	];

RHQ_ATInf_BAF = 
	[
	"BAF_Soldier_AT_MTP",
	"BAF_Soldier_HAT_MTP",
	"BAF_Soldier_AT_DDPM",
	"BAF_Soldier_HAT_DDPM",
	"BAF_Soldier_AT_W",
	"BAF_Soldier_HAT_W"
	];

RHQ_ATInf_PMC = 
	[
	"Soldier_AT_PMC"
	];

RHQ_AAInf_OA = 
	[
	"M6_EP1",
	"ZSU_TK_EP1",
	"HMMWV_Avenger_DES_EP1",
	"Ural_ZU23_TK_GUE_EP1",
	"Ural_ZU23_TK_EP1",
	"TK_GUE_Soldier_AA_EP1",
	"US_Soldier_AA_EP1",
	"TK_Soldier_AA_EP1",
	"TK_INS_Soldier_AA_EP1"
	];

RHQ_AAInf_ACR = 
	[
	];

RHQ_AAInf_BAF = 
	[
	"BAF_Soldier_AT_MTP",
	"BAF_Soldier_HAT_MTP",
	"BAF_Soldier_AT_DDPM",
	"BAF_Soldier_HAT_DDPM",
	"BAF_Soldier_AT_W",
	"BAF_Soldier_HAT_W"
	];

RHQ_AAInf_PMC = 
	[
	"Soldier_AA_PMC"
	];

RHQ_Inf_OA = 
	[
	"TK_GUE_Soldier_TL_EP1",
	"TK_GUE_Soldier_MG_EP1",
	"TK_GUE_Soldier_4_EP1",
	"TK_GUE_Soldier_Sniper_EP1",
	"TK_GUE_Soldier_5_EP1",
	"TK_GUE_Soldier_AT_EP1",
	"TK_GUE_Soldier_2_EP1",
	"TK_GUE_Soldier_HAT_EP1",
	"TK_GUE_Soldier_AT_EP1",
	"TK_GUE_Soldier_AA_EP1",
	"TK_GUE_Soldier_EP1",
	"TK_GUE_Soldier_AR_EP1",
	"TK_GUE_Soldier_3_EP1",
	"TK_GUE_Bonesetter_EP1",
	"TK_GUE_Warlord_EP1",
	"TK_CIV_Takistani03_EP1",
	"US_Soldier_AA_EP1",
	"US_Soldier_AT_EP1",
	"US_Soldier_AAT_EP1",
	"US_Soldier_HAT_EP1",
	"US_Soldier_AHAT_EP1",
	"US_Soldier_AR_EP1",
	"US_Soldier_AAR_EP1",
	"US_Soldier_Crew_EP1",
	"US_Soldier_Engineer_EP1",
	"US_Soldier_GL_EP1",
	"US_Soldier_MG_EP1",
	"US_Soldier_AMG_EP1",
	"US_Soldier_Marksman_EP1",
	"US_Soldier_Medic_EP1",
	"US_Soldier_Officer_EP1",
	"US_Soldier_Pilot_EP1",
	"US_Pilot_Light_EP1",
	"US_Soldier_EP1",
	"US_Soldier_LAT_EP1",
	"US_Soldier_B_EP1",
	"US_Soldier_Sniper_EP1",
	"US_Soldier_SniperH_EP1",
	"US_Soldier_Sniper_NV_EP1",
	"US_Soldier_Light_EP1",
	"US_Soldier_Spotter_EP1",
	"US_Soldier_SL_EP1",
	"US_Soldier_TL_EP1",
	"TK_Soldier_AA_EP1",
	"TK_Soldier_AAT_EP1",
	"TK_Soldier_AMG_EP1",
	"TK_Soldier_HAT_EP1",
	"TK_Soldier_AR_EP1",
	"TK_Aziz_EP1",
	"TK_Commander_EP1",
	"TK_Soldier_Crew_EP1",
	"TK_Soldier_Engineer_EP1",
	"TK_Soldier_GL_EP1",
	"TK_Soldier_MG_EP1",
	"TK_Soldier_Medic_EP1",
	"TK_Soldier_Officer_EP1",
	"TK_Soldier_Pilot_EP1",
	"TK_Soldier_EP1",
	"TK_Soldier_B_EP1",
	"TK_Soldier_LAT_EP1",
	"TK_Soldier_AT_EP1",
	"TK_Soldier_Sniper_EP1",
	"TK_Soldier_SniperH_EP1",
	"TK_Soldier_Sniper_Night_EP1",
	"TK_Soldier_Night_1_EP1",
	"TK_Soldier_Night_2_EP1",
	"TK_Soldier_TWS_EP1",
	"TK_Soldier_Spotter_EP1",
	"TK_Soldier_SL_EP1",
	"TK_Special_Forces_MG_EP1",
	"TK_Special_Forces_EP1",
	"TK_Special_Forces_TL_EP1",
	"TK_INS_Soldier_AA_EP1",
	"TK_INS_Soldier_AR_EP1",
	"TK_INS_Bonesetter_EP1",
	"TK_INS_Soldier_MG_EP1",
	"TK_INS_Soldier_2_EP1",
	"TK_INS_Soldier_EP1",
	"TK_INS_Soldier_4_EP1",
	"TK_INS_Soldier_3_EP1",
	"TK_INS_Soldier_AAT_EP1",
	"TK_INS_Soldier_Sniper_EP1",
	"TK_INS_Soldier_TL_EP1",
	"TK_INS_Warlord_EP1",
	"TK_INS_Soldier_AT_EP1",
	"TK_GUE_Soldier_AAT_EP1",
	"GER_Soldier_MG_EP1",
	"GER_Soldier_Medic_EP1",
	"GER_Soldier_EP1",
	"GER_Soldier_Scout_EP1",
	"GER_Soldier_TL_EP1",
	"US_Delta_Force_AR_EP1",
	"US_Delta_Force_M14_EP1",
	"US_Delta_Force_MG_EP1",
	"US_Delta_Force_EP1",
	"US_Delta_Force_Assault_EP1",
	"US_Delta_Force_Marksman_EP1",
	"US_Delta_Force_Air_Controller_EP1",
	"US_Delta_Force_Medic_EP1",
	"US_Delta_Force_Night_EP1",
	"CZ_Soldier_AMG_DES_EP1",
	"CZ_Soldier_AT_DES_EP1",
	"CZ_Soldier_B_DES_EP1",
	"CZ_Soldier_DES_EP1",
	"CZ_Soldier_Light_DES_EP1",
	"CZ_Soldier_MG_DES_EP1",
	"CZ_Soldier_Office_DES_EP1",
	"CZ_Soldier_Pilot_EP1",
	"CZ_Soldier_SL_DES_EP1",
	"CZ_Soldier_Sniper_EP1",
	"CZ_Special_Forces_DES_EP1",
	"CZ_Special_Forces_GL_DES_EP1",
	"CZ_Special_Forces_MG_DES_EP1",
	"CZ_Special_Forces_Scout_DES_EP1",
	"CZ_Special_Forces_TL_DES_EP1",
	"Drake",
	"Drake_Light",
	"Graves",
	"Graves_Light",
	"Herrera",
	"Herrera_Light",
	"Pierce",
	"Pierce_Light",
	"UN_CDF_Soldier_AAT_EP1",
	"UN_CDF_Soldier_AMG_EP1",
	"UN_CDF_Soldier_Crew_EP1",
	"UN_CDF_Soldier_B_EP1",
	"UN_CDF_Soldier_AT_EP1",
	"UN_CDF_Soldier_Light_EP1",
	"UN_CDF_Soldier_SL_EP1",
	"UN_CDF_Soldier_Guard_EP1",
	"UN_CDF_Soldier_MG_EP1",
	"UN_CDF_Soldier_Officer_EP1",
	"UN_CDF_Soldier_Pilot_EP1",
	"UN_CDF_Soldier_EP1"
	];

RHQ_Inf_ACR = 
	[
	"CZ_Soldier_RPG_Ass_Wdl_ACR",
	"CZ_Soldier_MG2_Wdl_ACR",
	"CZ_Soldier_Crew_Wdl_ACR",
	"CZ_Soldier_Engineer_Wdl_ACR",
	"CZ_Soldier_805g_Wdl_ACR",
	"CZ_Soldier_MG_Wdl_ACR",
	"CZ_Sharpshooter_Wdl_ACR",
	"CZ_Soldier_Medic_Wdl_ACR",
	"CZ_Soldier_Officer_Wdl_ACR",
	"CZ_Soldier_Pilot_Wdl_ACR",
	"CZ_Soldier_Wdl_ACR",
	"CZ_Soldier_AT_Wdl_ACR",
	"CZ_Soldier_805_Wdl_ACR",
	"CZ_Soldier_RPG_Wdl_ACR",
	"CZ_Soldier_Sniper_ACR",
	"CZ_Soldier_Light_Wdl_ACR",
	"CZ_Soldier_Spotter_ACR",
	"CZ_Soldier_Leader_Wdl_ACR",
	"CZ_Soldier_Spec3_Wdl_ACR",
	"CZ_Soldier_Spec2_Wdl_ACR",
	"CZ_Soldier_Recon_Wdl_ACR",
	"CZ_Soldier_Spec_Demo_Wdl_ACR",
	"CZ_Soldier_Spec1_Wdl_ACR",
	"CZ_Soldier_Crew_Dst_ACR",
	"CZ_Soldier_RPG_Dst_ACR",
	"CZ_Soldier_medik_DES_EP1",
	"CZ_Sharpshooter_DES_ACR",
	"CZ_Soldier_MG2_Dst_ACR",
	"CZ_Soldier_RPG_Ass_Dst_ACR",
	"CZ_Soldier_805g_Dst_ACR",
	"CZ_Soldier_Spec_Demo_Dst_ACR",
	"CZ_Soldier_Engineer_Dst_ACR",
	"CZ_Soldier805_DES_ACR"
	];

RHQ_Inf_BAF = 
	[
	"BAF_Soldier_AA_MTP",
	"BAF_Soldier_AAA_MTP",
	"BAF_Soldier_AAT_MTP",
	"BAF_Soldier_AHAT_MTP",
	"BAF_Soldier_AAR_MTP",
	"BAF_Soldier_AMG_MTP",
	"BAF_Soldier_AT_MTP",
	"BAF_Soldier_HAT_MTP",
	"BAF_Soldier_AR_MTP",
	"BAF_crewman_MTP",
	"BAF_Soldier_EN_MTP",
	"BAF_Soldier_GL_MTP",
	"BAF_Soldier_FAC_MTP",
	"BAF_Soldier_MG_MTP",
	"BAF_Soldier_scout_MTP",
	"BAF_Soldier_Marksman_MTP",
	"BAF_Soldier_Medic_MTP",
	"BAF_Soldier_Officer_MTP",
	"BAF_Pilot_MTP",
	"BAF_Soldier_MTP",
	"BAF_ASoldier_MTP",
	"BAF_Soldier_L_MTP",
	"BAF_Soldier_N_MTP",
	"BAF_Soldier_SL_MTP",
	"BAF_Soldier_SniperN_MTP",
	"BAF_Soldier_SniperH_MTP",
	"BAF_Soldier_Sniper_MTP",
	"BAF_Soldier_spotter_MTP",
	"BAF_Soldier_spotterN_MTP",
	"BAF_Soldier_TL_MTP",
	"BAF_Soldier_AA_DDPM",
	"BAF_Soldier_AAA_DDPM",
	"BAF_Soldier_AAT_DDPM",
	"BAF_Soldier_AHAT_DDPM",
	"BAF_Soldier_AAR_DDPM",
	"BAF_Soldier_AMG_DDPM",
	"BAF_Soldier_AT_DDPM",
	"BAF_Soldier_HAT_DDPM",
	"BAF_Soldier_AR_DDPM",
	"BAF_crewman_DDPM",
	"BAF_Soldier_EN_DDPM",
	"BAF_Soldier_GL_DDPM",
	"BAF_Soldier_FAC_DDPM",
	"BAF_Soldier_MG_DDPM",
	"BAF_Soldier_scout_DDPM",
	"BAF_Soldier_Marksman_DDPM",
	"BAF_Soldier_Medic_DDPM",
	"BAF_Soldier_Officer_DDPM",
	"BAF_Pilot_DDPM",
	"BAF_Soldier_DDPM",
	"BAF_ASoldier_DDPM",
	"BAF_Soldier_L_DDPM",
	"BAF_Soldier_N_DDPM",
	"BAF_Soldier_SL_DDPM",
	"BAF_Soldier_TL_DDPM",
	"BAF_Soldier_AA_W",
	"BAF_Soldier_AAA_W",
	"BAF_Soldier_AAT_W",
	"BAF_Soldier_AHAT_W",
	"BAF_Soldier_AAR_W",
	"BAF_Soldier_AMG_W",
	"BAF_Soldier_AT_W",
	"BAF_Soldier_HAT_W",
	"BAF_Soldier_AR_W",
	"BAF_crewman_W",
	"BAF_Soldier_EN_W",
	"BAF_Soldier_GL_W",
	"BAF_Soldier_FAC_W",
	"BAF_Soldier_MG_W",
	"BAF_Soldier_scout_W",
	"BAF_Soldier_Marksman_W",
	"BAF_Soldier_Medic_W",
	"BAF_Soldier_Officer_W",
	"BAF_Pilot_W",
	"BAF_Soldier_W",
	"BAF_ASoldier_W",
	"BAF_Soldier_L_W",
	"BAF_Soldier_N_W",
	"BAF_Soldier_SL_W",
	"BAF_Soldier_SniperN_W",
	"BAF_Soldier_SniperH_W",
	"BAF_Soldier_Sniper_W",
	"BAF_Soldier_spotter_W",
	"BAF_Soldier_spotterN_W",
	"BAF_Soldier_TL_W"
	];

RHQ_Inf_PMC = 
	[
	"CIV_Contractor1_BAF",
	"CIV_Contractor2_BAF",
	"Soldier_Bodyguard_M4_PMC",
	"Soldier_Bodyguard_AA12_PMC",
	"Soldier_Bodyguard_M4_PMC",
	"Soldier_Sniper_PMC",
	"Soldier_Medic_PMC",
	"Soldier_MG_PMC",
	"Soldier_MG_PKM_PMC",
	"Soldier_AT_PMC",
	"Soldier_Engineer_PMC",
	"Soldier_GL_M16A2_PMC",
	"Soldier_M4A3_PMC",
	"Soldier_PMC",
	"Soldier_GL_PMC",
	"Soldier_Crew_PMC",
	"Soldier_Pilot_PMC",
	"Soldier_Sniper_KSVK_PMC",
	"Soldier_AA_PMC",
	"Soldier_TL_PMC",
	"Soldier_Sniper_PMC"
	];

RHQ_Art_OA = 
	[
	"D30_TK_GUE_EP1",
	"2b14_82mm_TK_GUE_EP1",
	"M1129_MC_EP1",
	"MLRS_DES_EP1",
	"M252_US_EP1",
	"M119_US_EP1",
	"GRAD_TK_EP1",
	"MAZ_543_SCUD_TK_EP1",
	"2b14_82mm_TK_EP1",
	"D30_TK_EP1",
	"2b14_82mm_TK_INS_EP1",
	"2b14_82mm_CZ_EP1"
	];

RHQ_Art_ACR = 
	[
	"RM70_ACR"
	];

RHQ_Art_BAF = 
	[
	];

RHQ_Art_PMC = 
	[
	];

RHQ_HArmor_OA = 
	[
	"T55_TK_GUE_EP1",
	"T34_TK_GUE_EP1",
	"M1A1_US_DES_EP1",
	"M1A2_US_TUSK_MG_EP1",
	"T34_TK_EP1",
	"T72_TK_EP1",
	"T55_TK_EP1"
	];

RHQ_HArmor_ACR = 
	[
	"T72_ACR"
	];

RHQ_HArmor_BAF = 
	[
	];

RHQ_HArmor_PMC = 
	[
	];

RHQ_MArmor_OA = 
	[
	"M2A2_EP1",
	"M2A3_EP1",
	"T34_TK_EP1",
	"T34_TK_GUE_EP1"
	];

RHQ_MArmor_ACR = 
	[
	"Pandur2_ACR",
	"BMP2_Des_ACR",
	"BMP2_ACR",
	"BVP1_TK_ACR"
	];

RHQ_MArmor_BAF = 
	[
	"BAF_FV510_D",
	"BAF_FV510_W"
	];

RHQ_MArmor_PMC = 
	[
	];

RHQ_LArmor_OA = 
	[
	"BRDM2_TK_GUE_EP1",
	"BRDM2_HQ_TK_GUE_EP1",
	"BRDM2_ATGM_TK_EP1",
	"M1130_CV_EP1",
	"M6_EP1",
	"M2A2_EP1",
	"M2A3_EP1",
	"M1126_ICV_M2_EP1",
	"M1126_ICV_mk19_EP1",
	"M1135_ATGMV_EP1",
	"M1128_MGS_EP1",
	"BMP2_TK_EP1",
	"BMP2_HQ_TK_EP1",
	"BMP2_UN_EP1",
	"BRDM2_TK_EP1",
	"BTR60_TK_EP1",
	"M113_TK_EP1",
	"ZSU_TK_EP1",
	"M113_UN_EP1"
	];

RHQ_LArmor_ACR = 
	[
	"BRDM2_Desert_ACR",
	"BRDM2_ACR",
	"BMP2_Des_ACR",
	"BMP2_ACR",
	"BVP1_TK_ACR",
	"Dingo_GL_Wdl_ACR",
	"Dingo_WDL_ACR",
	"Dingo_DST_ACR",
	"Dingo_GL_DST_ACR",
	"Pandur2_ACR"
	];

RHQ_LArmor_BAF = 
	[
	"BAF_FV510_D",
	"BAF_FV510_W"
	];

RHQ_LArmor_PMC = 
	[
	];

RHQ_LArmorAT_OA = 
	[
	"M2A2_EP1",
	"M2A3_EP1",
	"M1135_ATGMV_EP1",
	"M1128_MGS_EP1",
	"BMP2_TK_EP1",
	"BMP2_UN_EP1"
	];

RHQ_LArmorAT_ACR = 
	[
	"BMP2_Des_ACR",
	"BVP1_TK_ACR",
	"Pandur2_ACR",
	"BVP1_TK_GUE_ACR"
	];

RHQ_LArmorAT_BAF = 
	[
	];

RHQ_LArmorAT_PMC = 
	[
	];

RHQ_Cars_OA = 
	[
	"UralRefuel_TK_EP1",
	"UralReammo_TK_EP1",
	"UralRepair_TK_EP1",
	"BTR40_TK_GUE_EP1",
	"BTR40_MG_TK_GUE_EP1",
	"Pickup_PK_TK_GUE_EP1",
	"Offroad_SPG9_TK_GUE_EP1",
	"Offroad_DSHKM_TK_GUE_EP1",
	"V3S_TK_GUE_EP1",
	"V3S_Reammo_TK_GUE_EP1",
	"ATV_US_EP1",
	"ATV_CZ_EP1",
	"HMMWV_DES_EP1",
	"HMMWV_MK19_DES_EP1",
	"HMMWV_TOW_DES_EP1",
	"HMMWV_M998_crows_M2_DES_EP1",
	"HMMWV_M998_crows_MK19_DES_EP1",
	"HMMWV_M1151_M2_DES_EP1",
	"HMMWV_M998A2_SOV_DES_EP1",
	"HMMWV_Terminal_EP1",
	"HMMWV_M1035_DES_EP1",
	"HMMWV_Avenger_DES_EP1",
	"HMMWV_M1151_M2_CZ_DES_EP1",
	"M1030_US_DES_EP1",
	"MTVR_DES_EP1",
	"LandRover_MG_TK_EP1",
	"LandRover_SPG9_TK_EP1",
	"LandRover_Special_CZ_EP1",
	"TT650_TK_EP1",
	"SUV_TK_EP1",
	"UAZ_Unarmed_TK_EP1",
	"UAZ_AGS30_TK_EP1",
	"UAZ_MG_TK_EP1",
	"Ural_ZU23_TK_EP1",
	"V3S_TK_EP1",
	"V3S_Open_TK_EP1",
	"BTR40_TK_INS_EP1",
	"BTR40_MG_TK_INS_EP1",
	"LandRover_MG_TK_INS_EP1",
	"LandRover_SPG9_TK_INS_EP1",
	"LandRover_CZ_EP1",
	"Old_bike_TK_INS_EP1",
	"Ural_ZU23_TK_GUE_EP1",
	"SUV_UN_EP1",
	"UAZ_Unarmed_UN_EP1",
	"Ural_UN_EP1"
	];

RHQ_Cars_ACR = 
	[
	"M1114_DSK_ACR",
	"LandRover_Ambulance_ACR",
	"LandRover_ACR",
	"RM70_ACR",
	"T810_ACR",
	"T810A_MG_ACR",
	"T810_Open_ACR",
	"T810Reammo_ACR",
	"T810Refuel_ACR",
	"T810Repair_ACR",
	"T810_Des_ACR",
	"UAZ_Unarmed_ACR",
	"M1114_AGS_ACR",
	"T810_Open_Des_ACR",
	"T810Refuel_Des_ACR",
	"LandRover_Ambulance_Des_ACR"
	];

RHQ_Cars_BAF = 
	[
	"BAF_ATV_D",
	"BAF_Jackal2_GMG_D",
	"BAF_Jackal2_L2A1_D",
	"BAF_Offroad_D",
	"BAF_ATV_W",
	"BAF_Jackal2_GMG_W",
	"BAF_Jackal2_L2A1_W",
	"BAF_Offroad_W"
	];

RHQ_Cars_PMC = 
	[
	"SUV_PMC",
	"SUV_PMC_BAF",
	"ArmoredSUV_PMC"
	];

RHQ_Air_OA = 
	[
	"UH1H_TK_GUE_EP1",
	"A10_US_EP1",
	"AH64D_EP1",
	"AH6J_EP1",
	"AH6X_EP1",
	"C130J_US_EP1",
	"CH_47F_EP1",
	"MH6J_EP1",
	"MQ9PredatorB_US_EP1",
	"UH60M_EP1",
	"An2_TK_EP1",
	"L39_TK_EP1",
	"Mi24_D_TK_EP1",
	"Mi17_TK_EP1",
	"Su25_TK_EP1",
	"UH1H_TK_EP1",
	"Mi171Sh_CZ_EP1",
	"Mi171Sh_rockets_CZ_EP1",
	"Mi17_UN_CDF_EP1"
	];

RHQ_Air_ACR = 
	[
	"L159_ACR",
	"L39_ACR",
	"L39_2_ACR",
	"Mi24_D_CZ_ACR"
	];

RHQ_Air_BAF = 
	[
	"BAF_Apache_AH1_D",
	"CH_47F_BAF",
	"BAF_Merlin_HC3_D",
	"AW159_Lynx_BAF"
	];

RHQ_Air_PMC = 
	[
	"Ka137_PMC",
	"Ka137_MG_PMC",
	"Ka60_PMC",
	"Ka60_GL_PMC"
	];

RHQ_BAir_OA = 
	[
	];

RHQ_BAir_ACR = 
	[
	];

RHQ_BAir_BAF = 
	[
	];

RHQ_BAir_PMC = 
	[
	];

RHQ_RAir_OA = 
	[
	"AH6J_EP1",
	"AH6X_EP1",
	"MQ9PredatorB_US_EP1",
	"An2_TK_EP1"
	];

RHQ_RAir_ACR = 
	[
	];

RHQ_RAir_BAF = 
	[
	];

RHQ_RAir_PMC = 
	[
	"Ka137_PMC"
	];

RHQ_NCAir_OA = 
	[
	"C130J_US_EP1",
	"AH6X_EP1",
	"MH6J_EP1",
	"An2_TK_EP1"
	];

RHQ_NCAir_ACR = 
	[
	];

RHQ_NCAir_BAF = 
	[
	];

RHQ_NCAir_PMC = 
	[
	"Ka137_PMC"
	];

RHQ_Naval_OA = 
	[
	"SeaFox_EP1"
	];

RHQ_Naval_ACR = 
	[
	"PBX_ACR"
	];

RHQ_Naval_BAF = 
	[
	];

RHQ_Naval_PMC = 
	[
	];

RHQ_Static_OA = 
	[
	"AGS_TK_GUE_EP1",
	"D30_TK_GUE_EP1",
	"2b14_82mm_TK_GUE_EP1",
	"DSHKM_TK_GUE_EP1",
	"DSHkM_Mini_TriPod_TK_GUE_EP1",
	"SearchLight_TK_EP1",
	"SearchLight_TK_GUE_EP1",
	"SearchLight_US_EP1",
	"SearchLight_TK_INS_EP1",
	"SPG9_TK_GUE_EP1",
	"ZU23_TK_GUE_EP1",
	"M252_US_EP1",
	"M119_US_EP1",
	"M2StaticMG_US_EP1",
	"M2HD_mini_TriPod_US_EP1",
	"MK19_TriPod_US_EP1",
	"TOW_TriPod_US_EP1",
	"Igla_AA_pod_TK_EP1",
	"AGS_TK_EP1",
	"D30_TK_EP1",
	"KORD_high_TK_EP1",
	"KORD_TK_EP1",
	"Metis_TK_EP1",
	"2b14_82mm_TK_EP1",
	"ZU23_TK_EP1",
	"AGS_TK_INS_EP1",
	"D30_TK_INS_EP1",
	"DSHKM_TK_INS_EP1",
	"DSHkM_Mini_TriPod_TK_INS_EP1",
	"2b14_82mm_TK_INS_EP1",
	"SPG9_TK_INS_EP1",
	"ZU23_TK_INS_EP1",
	"WarfareBMGNest_PK_TK_EP1",
	"WarfareBMGNest_PK_TK_GUE_EP1",
	"AGS_UN_EP1",
	"AGS_CZ_EP1",
	"KORD_high_UN_EP1",
	"KORD_UN_EP1",
	"SearchLight_UN_EP1"
	];

RHQ_Static_ACR = 
	[
	"Rbs70_ACR"
	];

RHQ_Static_BAF = 
	[
	"BAF_GMG_Tripod_D",
	"BAF_GPMG_Minitripod_D",
	"BAF_L2A1_Minitripod_D",
	"BAF_L2A1_Tripod_D",
	"BAF_GMG_Tripod_W",
	"BAF_GPMG_Minitripod_W",
	"BAF_L2A1_Minitripod_W",
	"BAF_L2A1_Tripod_W"
	];

RHQ_Static_PMC = 
	[
	];

RHQ_StaticAA_OA = 
	[
	"ZU23_TK_GUE_EP1",
	"Stinger_Pod_US_EP1",
	"Igla_AA_pod_TK_EP1",
	"ZU23_TK_EP1",
	"ZU23_TK_INS_EP1"
	];

RHQ_StaticAA_ACR = 
	[
	"Rbs70_ACR"
	];

RHQ_StaticAA_BAF = 
	[
	];

RHQ_StaticAA_PMC = 
	[
	];

RHQ_StaticAT_OA = 
	[
	"SPG9_TK_GUE_EP1",
	"TOW_TriPod_US_EP1",
	"Metis_TK_EP1",
	"SPG9_TK_INS_EP1"
	];

RHQ_StaticAT_ACR = 
	[
	];

RHQ_StaticAT_BAF = 
	[
	];

RHQ_StaticAT_PMC = 
	[
	];

RHQ_Support_OA = 
	[
	"V3S_Reammo_TK_GUE_EP1",
	"V3S_Refuel_TK_GUE_EP1",
	"V3S_Repair_TK_GUE_EP1",
	"HMMWV_Ambulance_DES_EP1",
	"HMMWV_Ambulance_CZ_DES_EP1",
	"MtvrReammo_DES_EP1",
	"MtvrRefuel_DES_EP1",
	"MtvrRepair_DES_EP1",
	"M1133_MEV_EP1",
	"UH60M_MEV_EP1",
	"M113Ambul_TK_EP1",
	"UralSupply_TK_EP1",
	"UralReammo_TK_EP1",
	"UralRefuel_TK_EP1",
	"UralRepair_TK_EP1",
	"M113Ambul_UN_EP1"
	];

RHQ_Support_ACR = 
	[
	"LandRover_Ambulance_ACR",
	"T810Reammo_ACR",
	"T810Refuel_ACR",
	"T810Repair_ACR",
	"T810Repair_Des_ACR",
	"T810Reammo_Des_ACR",
	"T810Refuel_Des_ACR"
	];

RHQ_Support_BAF = 
	[
	];

RHQ_Support_PMC = 
	[
	];

RHQ_Cargo_OA = 
	[
	"UH1H_TK_GUE_EP1",
	"M1126_ICV_M2_EP1",
	"M1126_ICV_mk19_EP1",
	"M6_EP1","M2A2_EP1",
	"M2A3_EP1",
	"BTR60_TK_EP1",
	"M113_TK_EP1",
	"BMP2_TK_EP1",
	"V3S_TK_EP1",
	"V3S_Open_TK_EP1",
	"SUV_TK_EP1",
	"UAZ_Unarmed_TK_EP1",
	"BTR40_TK_INS_EP1",
	"CH_47F_EP1",
	"UH60M_EP1",
	"BRDM2_ATGM_TK_EP1",
	"LandRover_Special_CZ_EP1",
	"Mi171Sh_CZ_EP1",
	"Mi171Sh_rockets_CZ_EP1"
	];

RHQ_Cargo_ACR = 
	[
	"BMP2_ACR",
	"BRDM2_ACR",
	"BRDM2_Desert_ACR",
	"BRDM2_Desert_ACR",
	"BRDM2_ACR",
	"Dingo_GL_Wdl_ACR",
	"Dingo_WDL_ACR",
	"Dingo_DST_ACR",
	"Dingo_GL_DST_ACR",
	"LandRover_ACR",
	"T810_ACR",
	"T810A_MG_ACR",
	"T810_Open_ACR",
	"T810A_Des_MG_ACR",
	"Mi24_D_CZ_ACR"
	];

RHQ_Cargo_BAF = 
	[
	"BAF_Offroad_D",
	"BAF_Offroad_W",
	"BAF_FV510_D",
	"BAF_FV510_W"
	];

RHQ_Cargo_PMC = 
	[
	"SUV_PMC",
	"SUV_PMC_BAF",
	"ArmoredSUV_PMC",
	"Ka60_PMC",
	"Ka60_GL_PMC"
	];

RHQ_NCCargo_OA = 
	[
	"BTR40_TK_GUE_EP1",
	"V3S_TK_GUE_EP1",
	"MH6J_EP1",
	"HMMWV_DES_EP1",
	"MTVR_DES_EP1",
	"SUV_TK_EP1",
	"UAZ_Unarmed_TK_EP1",
	"V3S_TK_EP1",
	"V3S_Open_TK_EP1",
	"SUV_UN_EP1",
	"UAZ_Unarmed_UN_EP1",
	"Ural_UN_EP1"
	];

RHQ_NCCargo_ACR = 
	[
	"LandRover_ACR",
	"T810_ACR",
	"T810_Open_ACR",
	"T810_Open_Des_ACR",
	"UAZ_Unarmed_ACR"
	];

RHQ_NCCargo_BAF = 
	[
	];

RHQ_NCCargo_PMC = 
	[
	"SUV_PMC",
	"SUV_PMC_BAF",
	"Ka60_PMC"
	];

RHQ_Crew_OA = 
	[
	"US_Soldier_Crew_EP1",
	"US_Soldier_Pilot_EP1",
	"US_Pilot_Light_EP1",
	"TK_Soldier_Crew_EP1",
	"TK_Soldier_Pilot_EP1",
	"CZ_Soldier_Pilot_EP1",
	"UN_CDF_Soldier_Pilot_EP1"
	];

RHQ_Crew_ACR = 
	[
	"CZ_Soldier_Crew_Wdl_ACR",
	"CZ_Soldier_Pilot_Wdl_ACR",
	"CZ_Soldier_Crew_Dst_ACR"
	];

RHQ_Crew_BAF = 
	[
	"BAF_crewman_MTP",
	"BAF_Pilot_MTP",
	"BAF_crewman_DDPM",
	"BAF_Pilot_DDPM",
	"BAF_creWman_W",
	"BAF_Pilot_W"
	];

RHQ_Crew_PMC = 
	[
	"Soldier_Crew_PMC",
	"Soldier_Pilot_PMC"
	];