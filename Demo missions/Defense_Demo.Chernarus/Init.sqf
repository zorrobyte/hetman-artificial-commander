//for map markers
RydHQ_Debug = true;
RydHQ_DebugII = true;

//to keep Leader in defend mode
RydHQ_Order = "DEFEND";

//only, if at least 6 groups will be closest to given objective, that objective become a perimeter point
RydHQ_DefendObjectives = 2;

//to make all objectives "taken"
RydHQ_NObj = 5;

//additional garrison unit to keep area around this group with manning empty static, placing inside buildings and patrolling
RydHQ_Garrison = [G1];

//adding OA classnames
RydHQ_OALib = true;

//directions of each perimeter, where objectives are placed around Leader
RydHQ_DefFrontL = ["N","E"];//North-East
RydHQ_DefFront1 = ["N",""];//1st perimeter facing North
RydHQ_DefFront2 = ["E",""];//2nd perimeter facing East
RydHQ_DefFront3 = ["S",""];//3rd perimeter facing South
RydHQ_DefFront4 = ["W",""];//4th perimeter facing West
//nul = [] execVM "RydHQInit.sqf";