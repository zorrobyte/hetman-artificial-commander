All demos are for HAC addon version. For launching with script version pasted into mission folder, add to the mission init (best at the end of init.sqf file) such line:

nul = [] execVM "RydHQInit.sqf";



HAC_simpleDemo

This is demo with very simple mission for two Leaders AI vs AI. Incudes minimal needed config + debug markers active and couple of groups per side. 


HAC_complexDemo

Here are used some more advanced init config variables and more complex mission is set with three Leaders, limited control and fronts. 


BigBoss_simpleDemo

Here is very simple mission for 1 Leader and Big Boss. HC module on map gives you another markings activated via ctrl + space. 


BigBoss_Demo

Here is mission for 8 Leaders (4/4) and Big Boss for both sides. In this demo is available "teleprot" for player's unit, simply by map click. This unit is also invulnerable. HC module on map gives you another markings activated via ctrl + space. Included custom battlefield area trigger (RydBB_MC) setup. Are used init confis for make batle over the map more dynamic, so Leaders will ommit recon, will at once send troops to capture objectives, and groups will move faster. 


AirCargo_Demo

Showcase of air cargo system specificity. There is one Leader, one team and one chopper. Note used configs, that guarantees quick and reliable task with assigned cargo unit (capturing in this case). Wait and follow your's TL orders. 


AmmoDrop_Demo

Showcase of special kind of ammo support designated for infantry. There is one Leader, player's unit, group with memeber, that have no ammo, one chopper designated for ammo drops, and set up ammo depot with ammoboxes to be dropped in subsequent missions. Use Ctrl+Space for waypoint lines, and watch on map, what is happening during next several minutes ignoring any orders. When chopper arrive, it will drop chute with ammo box. Groups should go there and, with possible some delay, member without ammo should rearm. 


Artillery_Demo

Showcase of artillery controlled by HAC. Mission includes one Leader, player as FO, two targets and one howitzer. Go prone, keep targets in yours optics to be syre, that targets remain "known", then go to map and wait some minutes for artillery markers. SPLASH map text means, that it is nearly time for boom. 


Defense_Demo

This mission shows, how to set up with HAC defense perimeter around HQ with additional garrisoning feature. 


