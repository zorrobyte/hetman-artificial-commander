// Vehicle should alread have a position to move to.
// Wait a bit to let the unit start moving.
_veh = _this select 0;
_Soldiergroup = _this select 1;
_module = _this select 2;

_driver = driver _veh;
sleep 4;

waituntil {(unitready _driver) OR !(vehicle _driver == _veh)};
_pos = getpos _veh;

if ((typeOf _veh) isKindOf "helicopter") then
{

	_veh land "GET OUT";
	while {(speed _veh < 8) AND ((getpos _unit select 2) < 4)} do
	{
		sleep 0.4;
	};

	{unassignVehicle _x; [_x] ordergetin false; sleep 0.1;} foreach units _Soldiergroup;
	[_Soldiergroup, _pos, 400] call BIS_fnc_taskPatrol;
	sleep 30;
	_driver move getpos _module;
	sleep 5;
	waitUntil {unitReady driver _veh};
};
if ((typeOf _VEH) isKindOf "plane") then
{
	_jump = [_veh] execVM "scripts\epic_battle_system\scripts\scripts\eject_cargo.sqf";
};
if ((typeOf _VEH) isKindOf "car") then
{
	waitUntil {unitReady driver _veh};
	{unassignVehicle _x; [_x] ordergetin false; sleep 0.1;} foreach units _Soldiergroup;
	[_Soldiergroup, _pos, 400] call BIS_fnc_taskPatrol;
};
