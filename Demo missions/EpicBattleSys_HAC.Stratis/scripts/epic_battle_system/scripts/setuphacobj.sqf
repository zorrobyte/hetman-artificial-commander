// 0 = [_towns, _syncObjs, _townradius] execvm "setuphacobj.sqf";

_townarray = _this select 0;
_leader = _this select 1;
_module = _this select 2;
private ["_townarray", "_leader", "_module","_x","_mkr","_mkrname","_pos"];

switch (_leader) do {
	case leaderHQ: {
		{
			if (_module getvariable "hacautoplace") then {
			_pos = [(locationPosition (_townarray select (floor (random (count _townarray))))),100,200,10,0,2000,0] call BIS_fnc_findSafePos;
			_x setpos _pos;
			};
			if (_module getvariable "debug") then {
				_mkrname = format ["mkr_%1",vehiclevarname _x];
				_mkr = createmarker [_mkrname, position _x];
				_mkr setmarkercolor "colorgreen";
				_mkr setmarkertype "Dot";
				_mkr setmarkertext _mkrname;

			};
		} foreach [RydHQ_Obj1,RydHQ_Obj2,RydHQ_Obj3,RydHQ_Obj4,RydHQ_Sec1,RydHQ_Sec2];
	};	
	case leaderHQB: {
		{
			if (_module getvariable "hacautoplace") then {
			_pos = [(locationPosition (_townarray select (floor (random (count _townarray))))),100,200,10,0,2000,0] call BIS_fnc_findSafePos;
			_x setpos _pos;
			};
			if (_module getvariable "debug") then {
				_mkrname = format ["mkr_%1",vehiclevarname _x];
				_mkr = createmarker [_mkrname, position _x];
				_mkr setmarkercolor "colorgreen";
				_mkr setmarkertype "Dot";
				_mkr setmarkertext _mkrname;

			};
		} foreach [RydHQB_Obj1,RydHQB_Obj2,RydHQB_Obj3,RydHQB_Obj4,RydHQB_Sec1,RydHQB_Sec2];
	};	
	case leaderHQC: {
		{
			if (_module getvariable "hacautoplace") then {
			_pos = [(locationPosition (_townarray select (floor (random (count _townarray))))),100,200,10,0,2000,0] call BIS_fnc_findSafePos;
			_x setpos _pos;
			};
			if (_module getvariable "debug") then {
				_mkrname = format ["mkr_%1",vehiclevarname _x];
				_mkr = createmarker [_mkrname, position _x];
				_mkr setmarkercolor "colorgreen";
				_mkr setmarkertype "Dot";
				_mkr setmarkertext _mkrname;

			};
		} foreach [RydHQC_Obj1,RydHQC_Obj2,RydHQC_Obj3,RydHQC_Obj4,RydHQC_Sec1,RydHQC_Sec2];
	};	
	case leaderHQD: {
		{
			if (_module getvariable "hacautoplace") then {
			_pos = [(locationPosition (_townarray select (floor (random (count _townarray))))),100,200,10,0,2000,0] call BIS_fnc_findSafePos;
			_x setpos _pos;
			};
			if (_module getvariable "debug") then {
				_mkrname = format ["mkr_%1",vehiclevarname _x];
				_mkr = createmarker [_mkrname, position _x];
				_mkr setmarkercolor "colorgreen";
				_mkr setmarkertype "Dot";
				_mkr setmarkertext _mkrname;

			};
		} foreach [RydHQD_Obj1,RydHQD_Obj2,RydHQD_Obj3,RydHQD_Obj4,RydHQD_Sec1,RydHQD_Sec2];
	};	
	case leaderHQE: {
		{
			if (_module getvariable "hacautoplace") then {
			_pos = [(locationPosition (_townarray select (floor (random (count _townarray))))),100,200,10,0,2000,0] call BIS_fnc_findSafePos;
			_x setpos _pos;
			};
			if (_module getvariable "debug") then {
				_mkrname = format ["mkr_%1",vehiclevarname _x];
				_mkr = createmarker [_mkrname, position _x];
				_mkr setmarkercolor "colorgreen";
				_mkr setmarkertype "Dot";
				_mkr setmarkertext _mkrname;

			};
		} foreach [RydHQE_Obj1,RydHQE_Obj2,RydHQE_Obj3,RydHQE_Obj4,RydHQE_Sec1,RydHQE_Sec2];
	};	
	case leaderHQF: {
		{
			if (_module getvariable "hacautoplace") then {
			_pos = [(locationPosition (_townarray select (floor (random (count _townarray))))),100,200,10,0,2000,0] call BIS_fnc_findSafePos;
			_x setpos _pos;
			};
			if (_module getvariable "debug") then {
				_mkrname = format ["mkr_%1",vehiclevarname _x];
				_mkr = createmarker [_mkrname, position _x];
				_mkr setmarkercolor "colorgreen";
				_mkr setmarkertype "Dot";
				_mkr setmarkertext _mkrname;

			};
		} foreach [RydHQF_Obj1,RydHQF_Obj2,RydHQF_Obj3,RydHQF_Obj4,RydHQF_Sec1,RydHQF_Sec2];
	};	
	case leaderHQG: {
		{
			if (_module getvariable "hacautoplace") then {
			_pos = [(locationPosition (_townarray select (floor (random (count _townarray))))),100,200,10,0,2000,0] call BIS_fnc_findSafePos;
			_x setpos _pos;
			};
			if (_module getvariable "debug") then {
				_mkrname = format ["mkr_%1",vehiclevarname _x];
				_mkr = createmarker [_mkrname, position _x];
				_mkr setmarkercolor "colorgreen";
				_mkr setmarkertype "Dot";
				_mkr setmarkertext _mkrname;

			};
		} foreach [RydHQG_Obj1,RydHQG_Obj2,RydHQG_Obj3,RydHQG_Obj4,RydHQG_Sec1,RydHQG_Sec2];
	};	
	case leaderHQH: {
		{
			if (_module getvariable "hacautoplace") then {
			_pos = [(locationPosition (_townarray select (floor (random (count _townarray))))),100,200,10,0,2000,0] call BIS_fnc_findSafePos;
			_x setpos _pos;
			};
			if (_module getvariable "debug") then {
				_mkrname = format ["mkr_%1",vehiclevarname _x];
				_mkr = createmarker [_mkrname, position _x];
				_mkr setmarkercolor "colorgreen";
				_mkr setmarkertype "Dot";
				_mkr setmarkertext _mkrname;

			};
		} foreach [RydHQH_Obj1,RydHQH_Obj2,RydHQH_Obj3,RydHQH_Obj4,RydHQH_Sec1,RydHQH_Sec2];
	};
};


