scriptName "scripts\epic_battle_system\init.sqf";
/*
	File: init.sqf
	Author: granQ
	Modified for standalone use, and added features by: subroc
	Description:
	Initialize the Epic Battle System (EBS). Based on the ACM but for any unit addons.

	Use:
	Place a unit and a GameLogic in the editor and sync them together, name the GL somthing like "EBS1".
	" Add 0 = this execvm "scripts\epic_battle_system\init.sqf" " in the GL init.
	Parameter(s):
	_this: the Logic object representing the EBS.

	Additional parameters to set via setvariable:
	" this setvariable ["maxunits", 100]; " max number of units active at the same time by the module. Default=100
	" this setvariable ["townradius", 500]; " maximal distance to scan for towns to capture. Default=5000
	" this setvariable ["orbat", 500]; " maximal number of units this module will spawn. Default=9999
	" this setvariable ["debug", true]; " Will generate debug information in sidechat: module name, units alive, max number of spawned units at the same time, number of units left (orbat). Default=false
	" this setvariable ["minspawndist", 100]; " If enemy is with x Meters EBS module will not spawn units. Default=50
	" this setvariable ["hacenabled", true]; " Will enable HAC functionalty for the EBS module (Require leaders and gamelogics named according to HAC standards). Default=false
	" this setvariable ["hacautoplace", true]; " Will place HAC objectives randomly in all locations found within townradius var (Require "hacenabled" variable to be true). Default=false
*/

if (isServer) then 
{
	_this setVariable ["initDone", false];
	
	sleep 0.5; //Allow scripted EBS's to add units.
	
	//Register the EBS with this squad.
	private ["_syncObjs"];
	_syncObjs = synchronizedObjects _this;
	
	//Remove all synchronized Logics from the list.
	private ["_syncObjsTmp"];
	_syncObjsTmp = +_syncObjs;
	{
		if (("Logic" countType [_x]) == 1) then {_syncObjs = _syncObjs - [_x]};
	} forEach _syncObjsTmp;
	
	//Shut down if the group does not contain at least one other unit.
	if ((count _syncObjs) == 0) exitWith {debugLog "Log: [EBS] Shutting down. No units were synchronized to the EBS."};

	//Only the first unit's group is checked. We only allow one group synchronized with the EBS.
	private ["_grp"];
	_grp = group (_syncObjs select 0);
	_this setVariable ["grp", _grp];
	
	//Check to make sure no other EBS is active for this squad.
	private ["_cnt"];
	_cnt = {!isNil {_x getVariable "EBS"}} count (units _grp);
	
	if (_cnt == 0) then 
	{
		private ["_esbMain"];
		_esbMain = _this execVM "scripts\epic_battle_system\esb_engine.sqf";
		
		//TODO:
		//diag_debugFSM _fsm;
		
		//Register EBS with all units in the group.
		{
			_x setVariable ["EBS", _this];
		} 
		forEach (units _grp);
	} 
	else 
	{
		debugLog ["Log: [EBS] Shutting down. Another EBS was detected for this group."];
	};
};
