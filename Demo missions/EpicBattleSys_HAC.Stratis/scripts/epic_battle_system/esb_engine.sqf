// Epic Battlefield System Module made by granQ, edited for standalone use by subroc
_module = _this;
_maxunits = _module getvariable "maxunits";
_townradius = _module getvariable "townradius";
_orbat = _module getvariable "orbat";


if( isNil { _module getvariable "maxunits" } ) then { _module setvariable ["maxunits", 200]; };
if( isNil { _module getvariable "townradius" } ) then { _module setvariable ["townradius", 5000]; };
if( isNil { _module getvariable "minspawndist" } ) then { _module setvariable ["minspawndist", 50]; };
if( isNil { _module getvariable "orbat" } ) then { _module setvariable ["orbat", 9999]; };
if( isNil { _module getvariable "debug" } ) then { _module setvariable ["debug", false]; };
if( isNil { _module getvariable "hacautoplace" } ) then { _module setvariable ["hacautoplace", false]; };
if( isNil { _module getvariable "hacenabled" } ) then { _module setvariable ["hacenabled", false]; };


private ["_veh","_maxunits","_module","_Soldiergroup","_orbat","_tmporbat","_neworbat","_uNum","_townradius","_nearUnits"];
_syncObjs = (synchronizedObjects _this) select 0;
_faction = faction _syncObjs;
_side = side _syncObjs;

_soldierType = typeof _syncObjs;
_vehicleClass = getText (configFile >> "CfgVehicles" >> _soldierType >> "vehicleclass");

// player sidechat format ["EBS Engine: Started for %1 using %2", faction _syncObjs,_vehicleClass];

// Create list of vehicles and soldiers

_list = [];
_soldiers = [];
_crews = [];
_this setvariable ["aliveunits", 0];
_planes = [];
_transportplanes = [];
_helicopters = [];
_transporthelicopters = [];
_tanks = [];
_apc = [];
_wheeled_recon = [];
_transport = [];
_supportvehicles = [];
_cfg = configFile >> "CfgVehicles";
for "_i" from 0 to ((count _cfg)-1) do
{
    _configVeh = _cfg select _i;
    if (isClass _configVeh) then
    {
        if (getNumber(_configVeh >> "scope") == 2 && getText (_configVeh >> "faction") == _faction) then
        {
		// See to which list we will add it to:

		_type = configName _configVeh;
		_list = _list + [_configVeh];
		if (_type isKindOf "Man") then
		{
			if (getText (_configVeh >> "vehicleclass") == _vehicleClass) then
			{
				_soldiers = _soldiers + [_type];
			};
		};
		if (_type isKindOf "Plane") then
		{
			_crew = getText(_configVeh >> "crew");
			_crews = _crews + [_crew];
			if (getNumber(_configVeh >> "transportsoldier") > 4 ) then
			{
				_transportplanes = _transportplanes + [_type];		
			} else
			{
				_planes = _planes + [_type];
			};
		};
		if (_type isKindOf "Helicopter") then {

			if (getNumber(_configVeh >> "transportsoldier") < 10 ) then
			{
				_helicopters = _helicopters + [_type];	
			} else
			{
				_transporthelicopters = _transporthelicopters + [_type];
			};
			_crew = getText(_configVeh >> "crew");
			_crews = _crews + [_crew];
		};
		if ((_type isKindOf "Tank") && !(_type isKindOf "MLRS")) then
		{
			_crew = getText(_configVeh >> "crew");
			_crews = _crews + [_crew];
			if (getNumber(_configVeh >> "transportsoldier") > 2 ) then
			{
				_apc = _apc + [_type];		
			} else {
				_tanks = _tanks + [_type];
			};
		};
		if ((_type isKindOf "Car") && !(_type isKindOf "GRAD_Base")) then
		{
			if (getNumber(_configVeh >> "transportsoldier") > 4 ) then { _transport = _transport + [_type]; } else
			{
				if (count (getArray (_configVeh >> "Turrets" >> "MainTurret" >> "magazines")) > 0) then {_wheeled_recon = _wheeled_recon + [_type]};
			};
		};
        };
    };
};  
// Clean lists
_soldiers = _soldiers - _crews;
_transport = _transport - _supportvehicles;
sleep 0.2;
// player sidechat format ["EBS Engine: Lists generated", _helicopters,_vehicleClass];

// Locations
_towns = nearestLocations [getPos _syncObjs, ["NameCityCapital","NameCity","NameVillage","NameLocal","Strategic","Airport","CityCenter","StrongpointArea"],_module getvariable "townradius"];
sleep 0.2;
// player sidechat format ["EBS Engine: Towns generated %1", _towns];
_radie = (viewDistance / 2);

// Setup HAC Targets
if (_module getvariable "hacenabled") then {
	0 = [_towns, _syncObjs, _module] execvm "scripts\epic_battle_system\scripts\setuphacobj.sqf";
};
// This is where the magic happens.. AKA time to spawn some soldiers :P

while {alive _syncObjs} do
{
	_nearUnits = nearestObjects [_syncObjs, ["CAManBase"], _module getvariable "minspawndist"];
	_delay =  random 60;
	sleep _delay;

	// Check FPS to make sure the computer can handle it
	if ((_module getvariable "orbat") > 0) then {
	if (diag_fps > 15) then {
	if ((_module getvariable "aliveunits") < (_module getvariable "maxunits")) then {
	if ((_syncObjs countEnemy _nearUnits) == 0 ) then {
	// Roll the dice, this will choose what type we will spawn.
	
	_dice =	random 6;
	_spawn = "";
	if (_dice > 0) then { _spawn = "plane";};
	if (_dice > 1) then { _spawn = "chopper";};
	if (_dice > 1.5) then { _spawn = "tanks";};
	if (_dice > 2.5) then { _spawn = "troopsAssault";};
	if (_dice > 13) then { _spawn = "troopsDefend";};
	if (_dice > 4) then { _spawn = "patrol";};

	// Create a group and spawn the unit.
	_group = createGroup _side;

	_allowFighters = _module getVariable "planes";
	_allowHelicopters = _module getVariable "helicopters";
	_allowTanks = _module getVariable "tanks";
	_allowAPC = _module getVariable "apc";
	_allowTrucks = _module getVariable "trucks";
	_allowInfantry = _module getVariable "infantry";
	_allowPatrols = _module getVariable "patrols";

	// Sends an airplane out
	if ((_spawn == "plane") and (count _planes > 0) AND (isnil ("_allowFighters"))) then
	{
		_spawnVehType = _planes select (floor (random (count _planes)));
		_veh = createVehicle [_spawnVehType,position _syncObjs,[], 40, "FLY"];
		_VehCrewType = getText (configFile >> "CfgVehicles" >> _spawnVehType >> "crew");

		_vehCrew = _group createUnit [_VehCrewType, getPosATL _syncObjs, [], 100, "FORM"] ;   
		[_vehCrew] join _group;
		sleep 0.2;
		_vehCrew moveInDriver _veh;
	};



	// Sends a helicopter out
	if ((_spawn == "chopper") and (count _helicopters > 0) AND (isnil ("_allowHelicopters"))) then
	{
		_spawnVehType = _helicopters select (floor (random (count _helicopters)));
		_veh = createVehicle [_spawnVehType,position _syncObjs,[], 40, "FLY"];
		_VehCrewType = getText (configFile >> "CfgVehicles" >> _spawnVehType >> "crew");

		_vehCrew = _group createUnit [_VehCrewType, getPosATL _syncObjs, [], 100, "FORM"] ;   
		[_vehCrew] join _group;
		sleep 0.2;
		_vehCrew moveInDriver _veh;
	};

	// Sends tanks out
	if ((_spawn == "tanks") and (count _tanks > 0) AND (isnil ("_allowTanks"))) then
	{
		_spawnVehType = _tanks select (floor (random (count _tanks)));
		_VehCrewType = getText (configFile >> "CfgVehicles" >> _spawnVehType >> "crew");
		_i = 0;
		while {_i < 3} do
		{
			_i = _i + 1;
			_veh = createVehicle [_spawnVehType,getPosATL _syncObjs,[], 40, "FLY"];
			_vehCrew = _group createUnit [_VehCrewType, getPosATL _syncObjs, [], 100, "FORM"]; 
			_vehCrewGunner = _group createUnit [_VehCrewType, getPosATL _syncObjs, [], 100, "FORM"]; 
			[_vehCrew,_vehCrewGunner] join _group;
			sleep 0.2;
			_vehCrew moveInDriver _veh;
			_vehCrewGunner moveInGunner _veh;
		};
	};
	// Sends assaulting troops out
	if ((_spawn == "troopsAssault") and (count _transport > 0) AND (isnil ("_allowInfantry"))) then
	{
		// First select a means of transport, airplane/chopper/apc or just a truck.
		// We need to see which options we got.
		_spawnVehType = "";
		_vehRandom = random 6;
		if ((_vehRandom > 0) AND (count _transport > 0) AND (isnil ("_allowTrucks"))) then { _spawnVehType = _transport select (floor (random (count _transport)));};
		if ((_vehRandom > 2) AND (count _apc > 0) AND (isnil ("_allowAPC"))) then { _spawnVehType = _apc select (floor (random (count _apc)));};
		if ((_vehRandom > 5) AND (count _transporthelicopters > 0) AND (isnil ("_allowHelicopters"))) then {_spawnVehType = _transporthelicopters select (floor (random (count _transporthelicopters)));};
		if ((_vehRandom > 6) AND (count _transportplanes > 0) AND (isnil ("_allowFighters"))) then {_spawnVehType = _transportplanes select (floor (random (count _transportplanes)));};

		if (_spawnVehType  != "") then
		{
			_VehCrewType = getText (configFile >> "CfgVehicles" >> _spawnVehType >> "crew");
			_groupSize = getNumber (configFile >> "CfgVehicles" >> _spawnVehType >> "transportsoldier");

			_veh = createVehicle [_spawnVehType,getPosATL _syncObjs,[], 40, "FLY"];
			_vehCrew = _group createUnit [_VehCrewType, getPosATL _syncObjs, [], 100, "FORM"];
			[_vehCrew] join _group;
			_VehHasGunner = _veh emptyPositions "gunner";

			if (_VehHasGunner > 0) then 
			{
				_vehCrewGunner = _group createUnit [_VehCrewType, getPosATL _syncObjs, [], 100, "FORM"];
				[_vehCrewGunner] join _group;
				sleep 0.2;
				_vehCrewGunner moveInGunner _veh;
			};
			sleep 0.2;
			_vehCrew moveInDriver _veh;

			_Soldiergroup = createGroup _side;
			_i = 1;
			while {_i < _groupSize} do
			{
				_i = _i + 1;
				_soldierType = _soldiers select (floor (random (count _soldiers)));
				_Soldier = _Soldiergroup createUnit [_soldierType, getPosATL _syncObjs, [], 100, "FORM"];
				[_soldier] join _Soldiergroup;
				_soldier moveInCargo _veh;
			};
			_Unload = [_veh,_Soldiergroup,_module] execVM "scripts\epic_battle_system\scripts\moveAndUnload.sqf";
		} else
		{
			_groupSize = (random 6) + 6;
			_i = 1;
			while {_i < _groupSize} do
			{
				_i = _i + 1;
				_soldierType = _soldiers select (floor (random (count _soldiers)));
				_Soldier = _group createUnit [_soldierType, getPosATL _syncObjs, [], 100, "FORM"];
				[_soldier] join _group;
			};
		};
	};
	// Sends a patrol out
	if ((_spawn == "patrol") AND (isnil ("_allowPatrols"))) then
	{
		if (count _wheeled_recon > 0) then	
		{
			_spawnVehType = _wheeled_recon select (floor (random (count _wheeled_recon)));
			_veh = createVehicle [_spawnVehType,getPosATL _syncObjs,[], 40, "FLY"];
			_VehCrewType = getText (configFile >> "CfgVehicles" >> _spawnVehType >> "crew");
	
			_vehCrew = _group createUnit [_VehCrewType, getPosATL _syncObjs, [], 100, "FORM"] ;   
			_vehCrewGunner = _group createUnit [_VehCrewType, getPosATL _syncObjs, [], 100, "FORM"];
			[_vehCrew,_vehCrewGunner] join _group;
			sleep 0.2;
			_vehCrewGunner moveInGunner _veh;
			_vehCrew moveInDriver _veh;
		} else
		{
			_groupSize = (random 2) + 3;
			_i = 1;
			while {_i < _groupSize} do
			{
				_i = _i + 1;
				_soldierType = _soldiers select (floor (random (count _soldiers)));
				_Soldier = _group createUnit [_soldierType, getPosATL _syncObjs, [], 100, "FORM"];
				[_soldier] join _group;
			};
		};
	};
	
	_uNum = count units _group;
	_newaliveunits = (_module getvariable "aliveunits") + _uNum;
	_module setvariable ["aliveunits", _newaliveunits];
	_leader = leader _group;
	if (_module getvariable "hacenabled") then {
		0 = [_syncObjs, _leader] execvm "scripts\epic_battle_system\scripts\sendtohac.sqf";
	} else {
		_group Move locationPosition (_towns select (floor (random (count _towns))));
	};
	{
		_x addEventHandler ["killed",{
			_newaliveunits = (_module getvariable "aliveunits") - 1;
			_module setvariable ["aliveunits", _newaliveunits];
		}]
	} foreach units _group; 
	// player sidechat format ["EBS Engine: Spawn units %1", _spawn,_allowPatrols];
	if (_module getvariable "debug") then {
		player sidechat format ["%2 Engine: Alive units %1/%3. %4 Left", _module getvariable "aliveunits", _module, _module getvariable "maxunits", (_module getvariable "orbat") - 1];
	};
	};
	};
	_tmporbat = _module getvariable "orbat";
	_neworbat = _tmporbat - _uNum;
	_module setvariable ["orbat", _neworbat];
	};
	};
};