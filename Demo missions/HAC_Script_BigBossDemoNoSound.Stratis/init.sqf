//A3 HAC config
RHQ_Recon = [
"B_soldier_M_F",
"O_soldier_M_F"];  

RHQ_FO = [
"B_soldier_M_F",
"O_soldier_M_F"];

RHQ_Snipers = [
"B_soldier_M_F",
"O_soldier_M_F"];

RHQ_ATInf = [
"B_soldier_LAT_F",
"O_Soldier_LAT_F"];

RHQ_AAInf = []; 

RHQ_Inf = [
"B_medic_F",
"B_soldier_AR_F",
"B_soldier_exp_F",
"B_Soldier_F",
"B_Soldier_GL_F",
"B_Soldier_lite_F",
"B_Soldier_SL_F",
"B_Soldier_TL_F",
"O_medic_F",
"O_Soldier_AR_F",
"O_soldier_exp_F",
"O_Soldier_F",
"O_Soldier_GL_F",
"O_Soldier_lite_F",
"O_Soldier_SL_F",
"O_Soldier_TL_F"];

RHQ_HArmor = [];

RHQ_MArmor = [];

RHQ_LArmor = [
"B_MRAP_01_hmg_F",
"B_MRAP_01_gmg_F",
"O_MRAP_02_gmg_F",
"O_MRAP_02_hmg_F"];  

RHQ_LarmorAT = [
"O_MRAP_02_gmg_F",
"B_MRAP_01_gmg_F"];  

RHQ_Cars = [
"B_MRAP_01_F",
"B_MRAP_01_gmg_F",
"B_MRAP_01_hmg_F",
"O_MRAP_02_F",
"O_MRAP_02_gmg_F",
"O_MRAP_02_hmg_F",
"C_Offroad_01_F",
"B_Quadbike_01_F",
"O_Quadbike_01_F"];  

RHQ_Air = [
"B_Heli_Light_01_armed_F",
"O_Heli_Light_02_F",
"O_Heli_Light_02_unarmed_F",
"B_Heli_Light_01_F"];

RHQ_NCAir = [
"O_Heli_Light_02_unarmed_F",
"B_Heli_Light_01_F"];

RHQ_BAir = [];
RHQ_RAir = [];
RHQ_Naval = [
"B_Boat_Transport_01_F",
"O_Boat_Transport_01_F",
"O_Boat_Armed_01_hmg_F",
"B_Boat_Armed_01_minigun_F"];

RHQ_Static = [];

RHQ_StaticAA = [];

RHQ_StaticAT = [];

RHQ_Support = [];

RHQ_Med = [];

RHQ_Ammo = [];

RHQ_Fuel = [];

RHQ_Rep = [];

RHQ_Cargo = [
"B_MRAP_01_hmg_F",
"B_MRAP_01_gmg_F",
"O_MRAP_02_gmg_F",
"O_MRAP_02_hmg_F",
"B_Heli_Light_01_armed_F",
"B_MRAP_01_F",
"O_MRAP_02_F",
"O_Heli_Light_02_F",
"O_Heli_Light_02_unarmed_F",
"B_Heli_Light_01_F"]; 

RHQ_NCCargo = [
"O_Heli_Light_02_unarmed_F",
"B_Heli_Light_01_F",
"B_MRAP_01_F",
"O_MRAP_02_F"];  

RHQ_Crew = [
"B_Helipilot_F",
"O_helipilot_F"];


if (isNil "isHC") then {
	isHC = false
};
/*
if (isNil "_checkFor") then {
	_checkFor = true
};
*/



RydHQ_Debug = true;
RydHQB_Debug = true;
RydHQC_Debug = true;
RydHQD_Debug = true;
RydHQE_Debug = true;
RydHQF_Debug = true;
RydHQG_Debug = true;
RydHQH_Debug = true;

RydHQ_IdleOrd = false;
RydHQB_IdleOrd = false;
RydHQC_IdleOrd = false;
RydHQD_IdleOrd = false;
RydHQE_IdleOrd = false;
RydHQF_IdleOrd = false;
RydHQG_IdleOrd = false;
RydHQH_IdleOrd = false;

RydHQ_DebugII = true;
RydHQB_DebugII = true;
RydHQC_DebugII = true;
RydHQD_DebugII = true;
RydHQE_DebugII = true;
RydHQF_DebugII = true;
RydHQG_DebugII = true;
RydHQH_DebugII = true;

RydHQ_CargoFind = 200;
RydHQB_CargoFind = 200;
RydHQC_CargoFind = 200;
RydHQD_CargoFind = 200;
RydHQE_CargoFind = 200;
RydHQF_CargoFind = 200;
RydHQG_CargoFind = 200;
RydHQH_CargoFind = 200;

//RydHQ_PathFinding = 100;

RydHQ_Rush = true;

RydBBa_HQs = [leaderHQ,leaderHQB,leaderHQC,leaderHQD];
RydBBb_HQs = [leaderHQE,leaderHQF,leaderHQG,leaderHQH];

RydBB_Active = true;
RydBB_Debug = true;

//RydBBa_Str = [];
//RydBBb_Str = [];

//RydBB_CustomObjOnly = true;
RydBB_BBOnMap = false;
//Init HAC
nul = [] execVM "RydHQInit.sqf";
//[1250] execfsm "scripts\fpsmanager.fsm";