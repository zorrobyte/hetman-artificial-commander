RYD_BBArrRefresh = 
	{
	RydBB_arrA = [
		RydHQ_Friends,
		RydHQ_NCrewInfG,
		RydHQ_CarsG,
		(RydHQ_HArmorG + RydHQ_LArmorG),
		RydHQ_AirG,
		RydHQ_NCAirG + (RydHQ_NCCargoG - RydHQ_NCAirG) + (RydHQ_SupportG - (RydHQ_NCAirG + (RydHQ_NCCargoG - RydHQ_NCAirG))),
		RydHQ_CCurrent,
		RydHQ_CInitial,
		RydHQ_FValue,
		RydHQ_Morale,
		RydHQ_KnEnemiesG,
		RydHQ_EnInfG,
		RydHQ_EnCarsG,
		(RydHQ_EnHArmorG + RydHQ_EnLArmorG),
		RydHQ_EnAirG,
		RydHQ_EnNCAirG + (RydHQ_EnNCCargoG - RydHQ_EnNCAirG) + (RydHQ_EnSupportG - (RydHQ_EnNCAirG + (RydHQ_EnNCCargoG - RydHQ_EnNCAirG))),
		RydHQ_EValue
		];

	if not (isNil "leaderHQB") then 
		{
		RydBB_arrB = [
			RydHQB_Friends,
			RydHQB_NCrewInfG,
			RydHQB_CarsG,
			(RydHQB_HArmorG + RydHQB_LArmorG),
			RydHQB_AirG,
			RydHQB_NCAirG + (RydHQB_NCCargoG - RydHQB_NCAirG) + (RydHQB_SupportG - (RydHQB_NCAirG + (RydHQB_NCCargoG - RydHQB_NCAirG))),
			RydHQB_CCurrent,
			RydHQB_CInitial,
			RydHQB_FValue,
			RydHQB_Morale,
			RydHQB_KnEnemiesG,
			RydHQB_EnInfG,
			RydHQB_EnCarsG,
			(RydHQB_EnHArmorG + RydHQB_EnLArmorG),
			RydHQB_EnAirG,
			RydHQB_EnNCAirG + (RydHQB_EnNCCargoG - RydHQB_EnNCAirG) + (RydHQB_EnSupportG - (RydHQB_EnNCAirG + (RydHQB_EnNCCargoG - RydHQB_EnNCAirG))),
			RydHQB_EValue
			]
		};

	if not (isNil "leaderHQC") then 
		{
		RydBB_arrC = [
			RydHQC_Friends,
			RydHQC_NCrewInfG,
			RydHQC_CarsG,
			(RydHQC_HArmorG + RydHQC_LArmorG),
			RydHQC_AirG,
			RydHQC_NCAirG + (RydHQC_NCCargoG - RydHQC_NCAirG) + (RydHQC_SupportG - (RydHQC_NCAirG + (RydHQC_NCCargoG - RydHQC_NCAirG))),
			RydHQC_CCurrent,
			RydHQC_CInitial,
			RydHQC_FValue,
			RydHQC_Morale,
			RydHQC_KnEnemiesG,
			RydHQC_EnInfG,
			RydHQC_EnCarsG,
			(RydHQC_EnHArmorG + RydHQC_EnLArmorG),
			RydHQC_EnAirG,
			RydHQC_EnNCAirG + (RydHQC_EnNCCargoG - RydHQC_EnNCAirG) + (RydHQC_EnSupportG - (RydHQC_EnNCAirG + (RydHQC_EnNCCargoG - RydHQC_EnNCAirG))),
			RydHQC_EValue
			]
		};

	if not (isNil "leaderHQD") then 
		{
		RydBB_arrD = [
			RydHQD_Friends,
			RydHQD_NCrewInfG,
			RydHQD_CarsG,
			(RydHQD_HArmorG + RydHQD_LArmorG),
			RydHQD_AirG,
			RydHQD_NCAirG + (RydHQD_NCCargoG - RydHQD_NCAirG) + (RydHQD_SupportG - (RydHQD_NCAirG + (RydHQD_NCCargoG - RydHQD_NCAirG))),
			RydHQD_CCurrent,
			RydHQD_CInitial,
			RydHQD_FValue,
			RydHQD_Morale,
			RydHQD_KnEnemiesG,
			RydHQD_EnInfG,
			RydHQD_EnCarsG,
			(RydHQD_EnHArmorG + RydHQD_EnLArmorG),
			RydHQD_EnAirG,
			RydHQD_EnNCAirG + (RydHQD_EnNCCargoG - RydHQD_EnNCAirG) + (RydHQD_EnSupportG - (RydHQD_EnNCAirG + (RydHQD_EnNCCargoG - RydHQD_EnNCAirG))),
			RydHQD_EValue
			]
		};

	if not (isNil "leaderHQE") then 
		{
		RydBB_arrE = [
			RydHQE_Friends,
			RydHQE_NCrewInfG,
			RydHQE_CarsG,
			(RydHQE_HArmorG + RydHQE_LArmorG),
			RydHQE_AirG,
			RydHQE_NCAirG + (RydHQE_NCCargoG - RydHQE_NCAirG) + (RydHQE_SupportG - (RydHQE_NCAirG + (RydHQE_NCCargoG - RydHQE_NCAirG))),
			RydHQE_CCurrent,
			RydHQE_CInitial,
			RydHQE_FValue,
			RydHQE_Morale,
			RydHQE_KnEnemiesG,
			RydHQE_EnInfG,
			RydHQE_EnCarsG,
			(RydHQE_EnHArmorG + RydHQE_EnLArmorG),
			RydHQE_EnAirG,
			RydHQE_EnNCAirG + (RydHQE_EnNCCargoG - RydHQE_EnNCAirG) + (RydHQE_EnSupportG - (RydHQE_EnNCAirG + (RydHQE_EnNCCargoG - RydHQE_EnNCAirG))),
			RydHQE_EValue
			]
		};

	if not (isNil "leaderHQF") then 
		{
		RydBB_arrF = [
			RydHQF_Friends,
			RydHQF_NCrewInfG,
			RydHQF_CarsG,
			(RydHQF_HArmorG + RydHQF_LArmorG),
			RydHQF_AirG,
			RydHQF_NCAirG + (RydHQF_NCCargoG - RydHQF_NCAirG) + (RydHQF_SupportG - (RydHQF_NCAirG + (RydHQF_NCCargoG - RydHQF_NCAirG))),
			RydHQF_CCurrent,
			RydHQF_CInitial,
			RydHQF_FValue,
			RydHQF_Morale,
			RydHQF_KnEnemiesG,
			RydHQF_EnInfG,
			RydHQF_EnCarsG,
			(RydHQF_EnHArmorG + RydHQF_EnLArmorG),
			RydHQF_EnAirG,
			RydHQF_EnNCAirG + (RydHQF_EnNCCargoG - RydHQF_EnNCAirG) + (RydHQF_EnSupportG - (RydHQF_EnNCAirG + (RydHQF_EnNCCargoG - RydHQF_EnNCAirG))),
			RydHQF_EValue
			]
		};

	if not (isNil "leaderHQG") then 
		{
		RydBB_arrG = [
			RydHQG_Friends,
			RydHQG_NCrewInfG,
			RydHQG_CarsG,
			(RydHQG_HArmorG + RydHQG_LArmorG),
			RydHQG_AirG,
			RydHQG_NCAirG + (RydHQG_NCCargoG - RydHQG_NCAirG) + (RydHQG_SupportG - (RydHQG_NCAirG + (RydHQG_NCCargoG - RydHQG_NCAirG))),
			RydHQG_CCurrent,
			RydHQG_CInitial,
			RydHQG_FValue,
			RydHQG_Morale,
			RydHQG_KnEnemiesG,
			RydHQG_EnInfG,
			RydHQG_EnCarsG,
			(RydHQG_EnHArmorG + RydHQG_EnLArmorG),
			RydHQG_EnAirG,
			RydHQG_EnNCAirG + (RydHQG_EnNCCargoG - RydHQG_EnNCAirG) + (RydHQG_EnSupportG - (RydHQG_EnNCAirG + (RydHQG_EnNCCargoG - RydHQG_EnNCAirG))),
			RydHQG_EValue
			]
		};

	if not (isNil "leaderHQH") then 
		{
		RydBB_arrH = [
			RydHQH_Friends,
			RydHQH_NCrewInfG,
			RydHQH_CarsG,
			(RydHQH_HArmorG + RydHQH_LArmorG),
			RydHQH_AirG,
			RydHQH_NCAirG + (RydHQH_NCCargoG - RydHQH_NCAirG) + (RydHQH_SupportG - (RydHQH_NCAirG + (RydHQH_NCCargoG - RydHQH_NCAirG))),
			RydHQH_CCurrent,
			RydHQH_CInitial,
			RydHQH_FValue,
			RydHQH_Morale,
			RydHQH_KnEnemiesG,
			RydHQH_EnInfG,
			RydHQH_EnCarsG,
			(RydHQH_EnHArmorG + RydHQH_EnLArmorG),
			RydHQH_EnAirG,
			RydHQH_EnNCAirG + (RydHQH_EnNCCargoG - RydHQH_EnNCAirG) + (RydHQH_EnSupportG - (RydHQH_EnNCAirG + (RydHQH_EnNCCargoG - RydHQH_EnNCAirG))),
			RydHQH_EValue
			]
		}
	};

RYD_AngTowards = 
	{
	private ["_source0", "_target0", "_rnd0","_dX0","_dY0","_angleAzimuth0"];
	_source0 = _this select 0;
	_target0 = _this select 1;
	_rnd0 = _this select 2;

	_dX0 = (_target0 select 0) - (_source0 select 0);
	_dY0 = (_target0 select 1) - (_source0 select 1);

	_angleAzimuth0 = (_dX0 atan2 _dY0) + (random (_rnd0 * 2)) - _rnd0;

	_angleAzimuth0
	};

RYD_DistOrdB = 
	{
	private ["_array","_first","_point","_dst","_limit","_final","_VL"];

	_array = _this select 0;//BB strategic areas
	_point = _this select 1;
	_limit = _this select 2;

	_first = [];
	_final = [];

		{
		_dst = round ((_x select 0) distance _point);
		if (_dst <= _limit) then {_first set [_dst,_x]}
		}
	foreach _array;

		{
		if not (isNil "_x") then {_final set [(count _final),_x]}
		}
	foreach _first;

	_first = nil;

	_final
	};

RYD_WhereIs = 
	{
	private ["_point","_Rpoint","_angle","_diffA","_axis","_isLeft","_isFlanking","_isBehind"];	

	_point = _this select 0;
	_rPoint = _this select 1;
	_axis = _this select 2;

	_angle = [_rPoint,_point,0] call RYD_AngTowards;

	_isLeft = false;
	_isFlanking = false;
	_isBehind = false;

	if (_angle < 0) then {_angle = _angle + 360};
	if (_axis < 0) then {_axis = _axis + 360};

	_diffA = _angle - _axis;

	if (_diffA < 0) then {_diffA = _diffA + 360};

	if (_diffA > 180) then 
		{
		_isLeft = true
		};

	if ((_diffA > 60) and (_diffA < 300)) then 
		{
		_isFlanking = true
		};

	if ((_diffA > 120) and (_diffA < 240)) then 
		{
		_isBehind = true
		};

	[_isLeft,_isFlanking,_isBehind]
	};

RYD_Sectorize = 
	{
	private ["_ctr","_lng","_ang","_nbr","_EdgeL","_rd","_main","_step","_X1","_Y1","_posX","_posY","_centers","_first",
	"_sectors","_centers2","_Xa","_Ya","_dXa","_dYa","_dst","_ang2","_Xb","_Yb","_dXb","_dYb","_center","_crX","_crY","_crPoint","_sec"];

	_ctr = _this select 0;
	_lng = _this select 1;
	_ang = _this select 2;
	_nbr = _this select 3;

	_EdgeL = _lng/_nbr;
	
	_rd = _lng/2;

	_main = createLocation ["Name", _ctr, _rd, _rd];
	_main setRectangular true;

	_step = _EdgeL;

	_X1 = _ctr select 0;
	_Y1 = _ctr select 1;

	_posX = (_X1 - _rd) + _step/2;
	_posY = (_Y1 - _rd) + _step/2;

	_centers = [[_posX,_posY]];
	_first = false;

	while {(true)} do
		{
		while {(true)} do
			{
			if not (_first) then {_first = true;_posX = _posX + _step};
			if not ([_posX,_PosY] in _main) exitwith {_posX = ((_ctr select 0) - _rd) + _step/2;_first = true};
			_centers set [(count _centers),[_posX,_PosY]];
			_first = false
			};
		_posY = _posY + _step;
		if not ([_posX,_PosY] in _main) exitwith {}
		};

	if not (_ang in [0,90,180,270]) then
		{
		_main setDirection _ang;
		_centers2 = _centers;
		_centers = [];

			{
			_Xa = _x select 0;
			_Ya = _x select 1;
			_dXa = (_X1 - _Xa);
			_dYa = (_Y1 - _Ya);
			_dst = _ctr distance _x;

			_ang2 = _ang + (_dXa atan2 _dYa);

			_dXb = _dst * (sin _ang2);
			_dYb = _dst * (cos _ang2);

			_Xb = _X1 + _dXb;
			_Yb = _Y1 + _dYb;
			_center = [_Xb,_Yb];
			_centers set [(count _centers),_center]
			}
		foreach _centers2
		};
	
	_sectors = [];

		{
		_crX = _x select 0;
		_crY = _x select 1;
		_crPoint = [_crX,_crY,0];
		_sec = createLocation ["Name", _crPoint, _EdgeL/2, _EdgeL/2];
		_sec setDirection _ang;
		_sec setRectangular true;

		_sectors set [(count _sectors),_sec];
		}
	foreach _centers;

	[_sectors,_main]	
	};

RYD_Marker = 
	{
	private ["_name","_pos","_cl","_shape","_size","_dir","_alpha","_type","_brush","_text","_i"];	

	_name = _this select 0;
	_pos = _this select 1;
	_cl = _this select 2;
	_shape = _this select 3;

	_shape = toUpper (_shape);

	_size = _this select 4;
	_dir = _this select 5;
	_alpha = _this select 6;

	if not (_shape == "ICON") then {_brush = _this select 7} else {_type = _this select 7};
	_text = _this select 8;

	if not ((typename _pos) == "ARRAY") exitWith {};
	if ((_pos select 0) == 0) exitWith {};
	if ((count _pos) < 2) exitWith {};
//diag_log format ["BB mark: %1 pos: %2 col: %3 size: %4 dir: %5 text: %6",_name,_pos,_cl,_size,_dir,_text];
	if (isNil "_pos") exitWith {};

	_i = _name;
	_i = createMarker [_i,_pos];
	_i setMarkerColor _cl;
	_i setMarkerShape _shape;
	_i setMarkerSize _size;
	_i setMarkerDir _dir;
	if not (_shape == "ICON") then {_i setMarkerBrush _brush} else {_i setMarkerType _type};
	_i setMarkerAlpha _alpha;
	_i setmarkerText _text;

	_i
	};

RYD_LocLineTransform = 
	{
	private ["_loc","_p1","_p2","_space","_center","_angle","_r1","_r2"];

	_loc = _this select 0;
	_p1 = _this select 1;//ATL
	_p2 = _this select 2;//ATL
	_space = _this select 3;

	_center = [((_p1 select 0) + (_p2 select 0))/2,((_p1 select 1) + (_p2 select 1))/2,0];

	_angle = [_p1,_p2,0] call RYD_AngTowards;

	_r1 = _space;
	_r2 = (_center distance _p1) + _space;

	_loc setPosition _center;
	_loc setDirection _angle;
	_loc setSize [_r1,_r2];

	true
	};

RYD_LocMultiTransform = 
	{
	private ["_loc","_ps","_space","_center","_angle","_r1","_r2","_sx","_sy","_cnt","_dmax","_pf","_dst","_pfMain","_check","_indx","_pfbis","_dmaxbis","_cX","_cY","_allIn","_mpl","_pX","_pY"];

	_loc = _this select 0;
	_ps = _this select 1;//array of ATL
	_space = _this select 2;

	_sx = 0;
	_sy = 0;

		{
		_sx = _sx + (_x select 0);
		_sy = _sy + (_x select 1)
		}
	foreach _ps;

	_cnt = count _ps;

	if not (_cnt > 0) exitWith {};
		
	_center = [_sx/_cnt,_sy/_cnt,0];

	_pf = _ps select 0;

	_dmax = _center distance _pf;

	_indx = 0;

	for "_i" from 0 to ((count _ps) - 1) do
		{
		_check = _ps select _i;

		if (((typeName _check) == "ARRAY") and ((count _check) > 1)) then
			{
			_cX = _check select 0;
			_cY = _check select 1;

			_check = [_cX,_cY,0];

			_dst = _center distance _check;
			if (_dst > _dmax) then
				{
				_pf = _check;
				_indx = _i;
				_dmax = _dst
				}
			}
		};

	_pfMain = _pf;

	_ps set [_indx,"DeleteThis"];
	_ps = _ps - ["DeleteThis"];

	_pf = _ps select 0;

	_dmaxbis = _center distance _pf;

	for "_i" from 0 to ((count _ps) - 1) do
		{
		_check = _ps select _i;

		_cX = _check select 0;
		_cY = _check select 0;

		_check = [_cX,_cY,0];

		_dst = _center distance _check;
		if (_dst > _dmaxbis) then
			{
			_dmaxbis = _dst
			}
		};

	_angle = [_center,_pfMain,0] call RYD_AngTowards;

	_r1 = _dmaxbis;
	_r2 = _dmax;

	_loc setPosition _center;
	_loc setDirection _angle;
	_loc setSize [_r1,_r2];

	_allIn = false;

	_mpl = 10;

	while {(not (_allIn) and (_mpl > 0))} do
		{
		_allIn = true;

		_r1 = _dmaxbis/_mpl;
		_loc setSize [_r1,_r2];

			{
			_pX = _x select 0;
			_pY = _x select 1;

			if not ([_pX,_pY,0] in _loc) exitWith {_allIn = false};
			}
		foreach (_ps + [_pfMain]);

		_mpl = _mpl - 0.1;
		};

	_allIn = false;

	_mpl = 10;

	while {(not (_allIn) and (_mpl > 0))} do
		{
		_allIn = true;

		_r2 = _dmax/_mpl;
		_loc setSize [_r1,_r2];

			{
			_pX = _x select 0;
			_pY = _x select 1;

			if not ([_pX,_pY,0] in _loc) exitWith {_allIn = false};
			}
		foreach (_ps + [_pfMain]);

		_mpl = _mpl - 0.1;
		};

	_r1 = _r1 + _space;
	_r2 = _r2 + _space;

	_loc setSize [_r1,_r2];

	true
	};

RYD_TerraCognita = 
	{
	private ["_position","_posX","_posY","_radius","_precision","_sourcesCount","_urban","_forest","_hills","_flat","_sea","_valS","_value","_val0","_samples","_sGr","_hprev","_hcurr","_samplePos","_i","_rds"];	

	_position = _this select 0;
	_samples = _this select 1;
	_rds = 100;
	if ((count _this) > 2) then {_rds = _this select 2};

	if not ((typeName _position) == "ARRAY") then {_position = getPosATL _position};

	_posX = _position select 0;
	_posY = _position select 1;

	_radius = 5;
	_precision = 1;
	_sourcesCount = 1;

	_urban = 0;
	_forest = 0;
	_hills = 0;
	_flat = 0;
	_sea = 0;

	_sGr = 0;
	_hprev = getTerrainHeightASL [_posX,_posY];

	for "_i" from 1 to 10 do
		{
		_samplePos = [_posX + ((random (_rds * 2)) - _rds),_posY + ((random (_rds * 2)) - _rds)];
		_hcurr = getTerrainHeightASL _samplePos;
		_sGr = _sGr + abs (_hcurr - _hprev)
		};

	_sGr = _sGr/10;

		{
		_valS = 0;

		for "_i" from 1 to _samples do
			{
			_position = [_posX + (random (_rds/5)) - (_rds/10),_posY + (random (_rds/5)) - (_rds/10)];


			_value = selectBestPlaces [_position,_radius,_x,_precision,_sourcesCount];

			_val0 = _value select 0;
			_val0 = _val0 select 1;

			_valS = _valS + _val0;
			};

		_valS = _valS/_samples;

		switch (_x) do
			{
			case ("Houses") : {_urban = _urban + _valS};
			case ("Trees") : {_forest = _forest + (_valS/3)};
			case ("Forest") : {_forest = _forest + _valS};
			case ("Hills") : {_hills = _hills + _valS};
			case ("Meadow") : {_flat = _flat + _valS};
			case ("Sea") : {_sea = _sea + _valS};
			};
		}
	foreach ["Houses","Trees","Forest","Hills","Meadow","Sea"];

	[_urban,_forest,_hills,_flat,_sea,_sGr]
	};

RYD_ForceCount = 
	{
	private ["_friends","_inf","_car","_arm","_air","_nc","_current","_initial","_value","_morale","_enemies","_einf","_ecar","_earm","_eair","_enc","_frArr","_enArr",
	"_eInfG","_eCarG","_eArmG","_eAirG","_eNCG","_eAllP","_eInfP","_eCarP","_eArmP","_eAirP","_eNCP","_allP","_infP","_carP","_armP","_airP","_ncP","_enG","_evalue",
	"_frRep","_enRep","_gpHQ"];

	_friends = _this select 0;
	_inf = _this select 1;
	_car = _this select 2;
	_arm = _this select 3;
	_air = _this select 4;
	_nc = _this select 5;

	_current = _this select 6;
	_initial = _this select 7;
	_value = _this select 8;
	_morale = _this select 9;

	_enemies = _this select 10;
	_einf = _this select 11;
	_ecar = _this select 12;
	_earm = _this select 13;
	_eair = _this select 14;
	_enc = _this select 15;
	_evalue = _this select 16;

	_frArr = _this select 17;
	_enArr = _this select 18;

	_enG = _this select 19;
	_gpHQ = _this select 20;

	_eInfG = [];
	_eCarG = [];
	_eArmG = [];
	_eAirG = [];
	_eNCG = [];	

	_eInfP = 0;
	_eCarP = 0;
	_eArmP = 0;
	_eAirP = 0;
	_eNCP = 0;

	_infP = 0;
	_carP = 0;
	_armP = 0;
	_airP = 0;
	_ncP = 0;

	if ((count _enemies) > 0) then 
		{
			{
			if (not (_x in _enG) and (_x in _einf)) then {_eInfG set [(count _eInfG),_x]};
			if (not (_x in _enG) and (_x in _ecar)) then {_eCarG set [(count _eCarG),_x]};
			if (not (_x in _enG) and (_x in _earm)) then {_eArmG set [(count _eArmG),_x]};
			if (not (_x in _enG) and (_x in _eair)) then {_eAirG set [(count _eAirG),_x]};
			if (not (_x in _enG) and (_x in _enc)) then {_eNCG set [(count _eNCG),_x]};	
			}
		foreach _enemies;

		_eAllP = {not (_x in _enG)} count _enemies;

		if (_eAllP > 0) then
			{
			_eInfP = (count _eInf)/_eAllP;
			_eCarP = (count _eCar)/_eAllP;
			_eArmP = (count _eArm)/_eAllP;
			_eAirP = (count _eAir)/_eAllP;
			_eNCP = (count _eNC)/_eAllP
			}
		};
		
	_allP = count _friends;

	if (_allP > 0) then
		{	
		_infP = (count _inf)/_allP;
		_carP = (count _car)/_allP;
		_armP = (count _arm)/_allP;
		_airP = (count _air)/_allP;
		_ncP = (count _nc)/_allP
		};

	_frRep = [_allP,_current,_current - _initial,_value,_morale,[_infP,_carP,_armP,_airP,_ncP]];//liczba grup-liczba jednostek-straty-wartosc-morale-rozklad
	_enRep = [count _enemies,_evalue,[_eInfP,_eCarP,_eArmP,_eAirP,_eNCP]];//liczba grup-wartosc-rozklad

	_gpHQ setVariable ["ForceRep",[_frRep,_enRep]];

	_frArr set [(count _frArr),_frRep];
	_enArr set [(count _enArr),_enRep];

	[_frArr,_enArr]
	};

RYD_ForceAnalyze = 
	{
	private ["_HQarr","_frArr","_enArr","_frG","_enG","_HQs","_arr"];

	_HQarr = _this select 0;

	_frArr = [];
	_enArr = [];

	_frG = [];
	_enG = [];

	_HQs = [];

		{
		switch (true) do
			{
			case ((_x == leaderHQ) and not (isNull RydHQ)) : 
				{
				_arr = (RydBB_arrA + [_frArr,_enArr,_enG,RydHQ]) call RYD_ForceCount;
				_frArr = _arr select 0;
				_enArr = _arr select 1;
				_HQs set [(count _HQs),(group _x)];
				_frG = _frG + RydHQ_Friends - RydHQ_Exhausted;

					{
					if not (_x in _enG) then {_enG set [(count _enG),_x]};
					}
				foreach RydHQ_KnEnemiesG
				};

			case ((_x == leaderHQB) and not (isNull RydHQB)) : 
				{
				_arr = (RydBB_arrB + [_frArr,_enArr,_enG,RydHQB]) call RYD_ForceCount;
				_frArr = _arr select 0;
				_enArr = _arr select 1;
				_HQs set [(count _HQs),(group _x)];
				_frG = _frG + RydHQB_Friends - RydHQB_Exhausted;

					{
					if not (_x in _enG) then {_enG set [(count _enG),_x]};
					}
				foreach RydHQB_KnEnemiesG
				};

			case ((_x == leaderHQC) and not (isNull RydHQC)) : 
				{
				_arr = (RydBB_arrC + [_frArr,_enArr,_enG,RydHQC]) call RYD_ForceCount;
				_frArr = _arr select 0;
				_enArr = _arr select 1;
				_HQs set [(count _HQs),(group _x)];
				_frG = _frG + RydHQC_Friends - RydHQC_Exhausted;

					{
					if not (_x in _enG) then {_enG set [(count _enG),_x]};
					}
				foreach RydHQC_KnEnemiesG
				};

			case ((_x == leaderHQD) and not (isNull RydHQD)) : 
				{
				_arr = (RydBB_arrD + [_frArr,_enArr,_enG,RydHQD]) call RYD_ForceCount;
				_frArr = _arr select 0;
				_enArr = _arr select 1;
				_HQs set [(count _HQs),(group _x)];
				_frG = _frG + RydHQD_Friends - RydHQD_Exhausted;

					{
					if not (_x in _enG) then {_enG set [(count _enG),_x]};
					}
				foreach RydHQD_KnEnemiesG
				};
			case ((_x == leaderHQE) and not (isNull RydHQE)) : 
				{
				_arr = (RydBB_arrE + [_frArr,_enArr,_enG,RydHQE]) call RYD_ForceCount;
				_frArr = _arr select 0;
				_enArr = _arr select 1;
				_HQs set [(count _HQs),(group _x)];
				_frG = _frG + RydHQE_Friends - RydHQE_Exhausted;

					{
					if not (_x in _enG) then {_enG set [(count _enG),_x]};
					}
				foreach RydHQE_KnEnemiesG
				};

			case ((_x == leaderHQF) and not (isNull RydHQF)) : 
				{
				_arr = (RydBB_arrF + [_frArr,_enArr,_enG,RydHQF]) call RYD_ForceCount;
				_frArr = _arr select 0;
				_enArr = _arr select 1;
				_HQs set [(count _HQs),(group _x)];
				_frG = _frG + RydHQF_Friends - RydHQF_Exhausted;

					{
					if not (_x in _enG) then {_enG set [(count _enG),_x]};
					}
				foreach RydHQF_KnEnemiesG
				};

			case ((_x == leaderHQG) and not (isNull RydHQG)) : 
				{
				_arr = (RydBB_arrG + [_frArr,_enArr,_enG,RydHQG]) call RYD_ForceCount;
				_frArr = _arr select 0;
				_enArr = _arr select 1;
				_HQs set [(count _HQs),(group _x)];
				_frG = _frG + RydHQG_Friends - RydHQG_Exhausted;

					{
					if not (_x in _enG) then {_enG set [(count _enG),_x]};
					}
				foreach RydHQG_KnEnemiesG
				};

			case ((_x == leaderHQH) and not (isNull RydHQH)) : 
				{
				_arr = (RydBB_arrH + [_frArr,_enArr,_enG,RydHQH]) call RYD_ForceCount;
				_frArr = _arr select 0;
				_enArr = _arr select 1;
				_HQs set [(count _HQs),(group _x)];
				_frG = _frG + RydHQH_Friends - RydHQH_Exhausted;

					{
					if not (_x in _enG) then {_enG set [(count _enG),_x]};
					}
				foreach RydHQH_KnEnemiesG
				};
			}
		}
	foreach _HQarr;

	_frArr set [(count _frArr),_frG];
	_enArr set [(count _enArr),_enG];

	[_frArr,_enArr,_HQs]
	};

RYD_TopoAnalize = 
	{
	private ["_sectors","_sectors0","_infF","_vehF","_ct","_urbanF","_forestF","_hillsF","_flatF","_seaF","_roadsF","_grF","_actInf","_actVeh"];

	_sectors = _this select 0;

	_sectors0 = _sectors;


	_infF = 0;
	_vehF = 0;
	_ct = 0;

		{
		_urbanF = _x getVariable "Topo_Urban";
		_forestF = _x getVariable "Topo_Forest";
		_hillsF = _x getVariable "Topo_Hills";
		_flatF = _x getVariable "Topo_Flat";
		_seaF = _x getVariable "Topo_Sea";
		_roadsF = _x getVariable "Topo_Roads";
		_grF = _x getVariable "Topo_Grd";

		if not (_seaF >= 90) then 
			{
			//diag_log format ["L - U: %1 F: %2 H: %3, Fl: %4 S: %5 R: %6 G: %7 ",_urbanF,_forestF,_hillsF,_flatF,_seaF,_roadsF,_grF];

			_actInf = _urbanF + _forestF + _grF - _flatF - _hillsF;
			_actVeh = _flatF + _hillsF + _roadsF - _urbanF - _forestF - _grF;

			_x setVariable ["InfFr",_actInf];
			_x setVariable ["VehFr",_actVeh];

			_infF = _infF + _actInf;
			_vehF = _vehF + _actVeh;

			//_txt = format ["Inf: %1 - Veh: %2",_urbanF + _forestF + _grF - _flatF - _hillsF,_flatF + _hillsF + _roadsF - _urbanF - _forestF - _grF];

			//_x setText _txt;
			_ct = _ct + 1
			}
		else
			{
			_sectors = _sectors - [_x];
			}
		}
	foreach _sectors0;

	if (_ct > 0) then {_infF = _infF/_ct};
	if (_ct > 0) then {_vehF = _vehF/_ct};

	[_sectors,_infF,_vehF]
	};

RYD_Itinerary = 
	{
	private ["_sectors","_targets","_pos1","_pos2","_bound","_secIn","_tgtIn","_topoAn","_infF","_vehF","_side","_cSum","_varName","_HandledArray"];	

	_sectors = _this select 0;
	_targets = _this select 1;
	_pos1 = _this select 2;
	_pos2 = _this select 3;
	_side = _this select 4;

	_bound = createLocation ["Name", _pos1, 1, 1];
	_bound setRectangular true;

	[_bound,_pos1,_pos2,1200] call RYD_LocLineTransform;

	_secIn = [];
	_tgtIn = [];

		{
		if ((position _x) in _bound) then {_secIn set [(count _secIn),_x]}
		}
	foreach _sectors;

		{
		if ((_x select 0) in _bound) then 
			{
			_cSum = 0;

				{
				_cSum = _cSum + _x
				}
			foreach (_x select 0);

			_varName = "HandledAreas" + _side;

			_HandledArray = missionNameSpace getVariable _varName;

			if (isNil "_HandledArray") then 
				{
				missionNameSpace setVariable [_varName,[]];
				_HandledArray = missionNameSpace getVariable _varName
				};

			if not (_cSum in _HandledArray) then 
				{
				_tgtIn set [(count _tgtIn),_x];
				_HandledArray set [(count _HandledArray),_cSum];
				missionNameSpace setVariable [_varName,_HandledArray];
				}
			}
		}
	foreach _targets;

	deleteLocation _bound;

	_topoAn = [_secIn] call RYD_TopoAnalize;

	_secIn = _topoAn select 0;

	_topoAn = [_secIn] call RYD_TopoAnalize;

	_infF = _topoAn select 1;
	_vehF = _topoAn select 2;

	[_secIn,_tgtIn,_infF,_vehF]
	};

RYD_ExecutePath = 
	{
	private ["_HQ","_areas","_o1","_o2","_o3","_o4","_allied","_HQpos","_sortedA","_i","_nObj","_actO","_nObj","_KnEn","_KnEnAct","_VLpos","_enX","_enY","_ct","_VHQpos","_front","_afront",
	"_frPos","_frDir","_frDim","_chosenPos","_maxTempt","_actTempt","_sectors","_ownKnEn","_ownForce","_ctOwn","_alliedForce","_alliedGarrisons","_alliedExhausted","_inFlank","_Garrisons","_exhausted",
	"_prop","_enPos","_dst","_val","_profile","_j","_pCnt","_m","_checkPos","_actPos","_indx","_check","_reserve","_garrPool","_fG","_garrison","_chosen","_dstMin","_actG","_actDst","_side",
	"_AllV","_Civs","_AllV2","_Civs2","_AllV0","_AllV20","_NearAllies","_NearEnemies","_actOPos","_mChange","_marksT","_firstP","_actP","_angleM","_centerPoint","_mr1","_mr2","_lM","_wp",
	"_varName","_HandledArray","_cSum","_reck","_cons","_limit","_lColor"];	

	_HQ = _this select 0;//leader units
	_areas = _this select 1;
	_o1 = _this select 2;
	_o2 = _this select 3;
	_o3 = _this select 4;
	_o4 = _this select 5;
	_allied = (_this select 6) - [_HQ];//leader units

	_HQpos = _this select 7;
	_front = _this select 8;
	_sectors = _this select 9;
	_reserve = _this select 10;
	_side = _this select 11;

	_varName = "HandledAreas" + _side;

	_HandledArray = missionNameSpace getVariable _varName;

	_frPos = position _front;
	_frDir = direction _front;
	_frDim = size _front;

	_profile = (group _HQ) getVariable "ForceProfile";

	_sortedA = [_areas,_HQpos,25000] call RYD_DistOrdB;

	_pCnt = 0;

	_m = "";
	_marksT = [];		

	if (RydBB_Debug) then
		{
			{
			_pCnt = _pCnt + 1;
			_j = [(_x select 0),(random 1000),"markBBPath","ColorBlack","ICON","mil_box",(str _pCnt),"",[0.35,0.35]] call RYD_Mark;
			_marksT set [(count _marksT),_j]
			}
		foreach _sortedA;

		for "_i" from 0 to ((count _sortedA) - 1) do
			{
			_firstP = _HQpos;
			if (_i > 0) then {_firstP = (_sortedA select (_i - 1)) select 0};

			_firstP = [_firstP select 0,_firstP select 1,0];

			_actP = (_sortedA select _i) select 0;
			_actP = [_actP select 0,_actP select 1,0];

			_angleM = [_firstP,_actP,0] call RYD_AngTowards;

			_centerPoint = [((_firstP select 0) + (_actP select 0))/2,((_firstP select 1) + (_actP select 1))/2,0];

			_mr1 = 1.5;
			_mr2 = _actP distance _centerPoint;

			_lM = [_centerPoint,(random 1000),"markBBline","ColorPink","RECTANGLE","Solid","","",[_mr1,_mr2],_angleM] call RYD_Mark;

			_marksT set [(count _marksT),_lM]
			}
		};

	(group _HQ) setVariable ["PathDone",false];

	for "_i" from 0 to ((count _sortedA) - 1) do
		{
		_actO = _sortedA select _i;

		_cSum = 0;

			{
			_cSum = _cSum + _x
			}
		foreach (_actO select 0);

		_actOPos = [(_actO select 0) select 0,(_actO select 0) select 1,0];
		_lColor = "ColorBlue";
		if (_Side == "B") then {_lColor = "ColorRed"};

		if ((RydBB_Debug) or ((RydBBa_SimpleDebug) and (_Side == "A")) or ((RydBBb_SimpleDebug) and (_Side == "B"))) then
			{
			if (_i == 0) then {_m = [(_actO select 0),_HQ,"markBBCurrent",_lColor,"ICON","mil_triangle","Current target for " + (str _HQ),"",[0.5,0.5]] call RYD_Mark} else {_m setMarkerPos (_actO select 0)};
			};

			{
			_x setPosATL _actOPos
			}
		foreach [_o1,_o2,_o3,_o4];

		(group _HQ) setVariable ["ObjInit",true];

		switch (_HQ) do
			{
			case (leaderHQ) : {RydHQ_NObj = 1};
			case (leaderHQB) : {RydHQB_NObj = 1};
			case (leaderHQC) : {RydHQC_NObj = 1};
			case (leaderHQD) : {RydHQD_NObj = 1};
			case (leaderHQE) : {RydHQE_NObj = 1};
			case (leaderHQF) : {RydHQF_NObj = 1};
			case (leaderHQG) : {RydHQG_NObj = 1};
			case (leaderHQH) : {RydHQH_NObj = 1};
			};

		waitUntil
			{
			sleep 120;

			_KnEn = [];

			_inFlank = (group _HQ) getVariable "inFlank";
			if (isNil "_inFlank") then {_inFlank = false};

			if not (_inFlank) then
				{
				_ownKnEn = RydHQ_KnEnemiesG;

				_ownForce = RydHQ_Friends;
				_Garrisons = RydHQ_Garrison;
				_exhausted = RydHQ_Exhausted;
				
				switch (_HQ) do
					{
					case (leaderHQB) : {_ownKnEn = RydHQB_KnEnemiesG;_ownForce = RydHQB_Friends;_Garrisons = RydHQB_Garrison;_exhausted = RydHQB_Exhausted};
					case (leaderHQC) : {_ownKnEn = RydHQC_KnEnemiesG;_ownForce = RydHQC_Friends;_Garrisons = RydHQC_Garrison;_exhausted = RydHQC_Exhausted};
					case (leaderHQD) : {_ownKnEn = RydHQD_KnEnemiesG;_ownForce = RydHQD_Friends;_Garrisons = RydHQD_Garrison;_exhausted = RydHQD_Exhausted};
					case (leaderHQE) : {_ownKnEn = RydHQE_KnEnemiesG;_ownForce = RydHQE_Friends;_Garrisons = RydHQE_Garrison;_exhausted = RydHQE_Exhausted};
					case (leaderHQF) : {_ownKnEn = RydHQF_KnEnemiesG;_ownForce = RydHQF_Friends;_Garrisons = RydHQF_Garrison;_exhausted = RydHQF_Exhausted};
					case (leaderHQG) : {_ownKnEn = RydHQG_KnEnemiesG;_ownForce = RydHQG_Friends;_Garrisons = RydHQG_Garrison;_exhausted = RydHQG_Exhausted};
					case (leaderHQH) : {_ownKnEn = RydHQH_KnEnemiesG;_ownForce = RydHQH_Friends;_Garrisons = RydHQH_Garrison;_exhausted = RydHQH_Exhausted};
					};

				if (isNil "_exhausted") then {_exhausted = []};

				_ownForce = _ownForce - (_Garrisons + _exhausted);

				_ctOwn = 0;

					{
					if ((position (vehicle (leader _x))) in _front) then {_ctOwn = _ctOwn + 1}
					}
				foreach _ownKnEn;

				_prop = 100;

				if (_ctOwn > 0) then {_prop = (count _ownForce)/_ctOwn};

				if (_prop > (8 * (0.5 + (random 1)))) then
					{
						{
					
						_KnEnAct = RydHQ_KnEnemiesG;
						_afront = FrontA;	

						_alliedForce = RydHQ_Friends;
						_alliedGarrisons = RydHQ_Garrison;
						_alliedExhausted = RydHQ_Exhausted;	

						switch (_x) do
							{
							case (leaderHQB) : {_KnEnAct = RydHQB_KnEnemiesG;_afront = FrontB;_alliedForce = RydHQB_Friends;_alliedGarrisons = RydHQB_Garrison;_alliedExhausted = RydHQB_Exhausted};
							case (leaderHQC) : {_KnEnAct = RydHQC_KnEnemiesG;_afront = FrontC;_alliedForce = RydHQC_Friends;_alliedGarrisons = RydHQC_Garrison;_alliedExhausted = RydHQC_Exhausted};
							case (leaderHQD) : {_KnEnAct = RydHQD_KnEnemiesG;_afront = FrontD;_alliedForce = RydHQD_Friends;_alliedGarrisons = RydHQD_Garrison;_alliedExhausted = RydHQD_Exhausted};
							case (leaderHQE) : {_KnEnAct = RydHQE_KnEnemiesG;_afront = FrontE;_alliedForce = RydHQE_Friends;_alliedGarrisons = RydHQE_Garrison;_alliedExhausted = RydHQE_Exhausted};
							case (leaderHQF) : {_KnEnAct = RydHQF_KnEnemiesG;_afront = FrontF;_alliedForce = RydHQF_Friends;_alliedGarrisons = RydHQF_Garrison;_alliedExhausted = RydHQF_Exhausted};
							case (leaderHQG) : {_KnEnAct = RydHQG_KnEnemiesG;_afront = FrontG;_alliedForce = RydHQG_Friends;_alliedGarrisons = RydHQG_Garrison;_alliedExhausted = RydHQG_Exhausted};
							case (leaderHQH) : {_KnEnAct = RydHQH_KnEnemiesG;_afront = FrontH;_alliedForce = RydHQH_Friends;_alliedGarrisons = RydHQH_Garrison;_alliedExhausted = RydHQH_Exhausted};
							};

						if (isNil "_alliedExhausted") then {_alliedExhausted = []};
						_alliedForce =  _alliedForce - (_alliedGarrisons + _alliedExhausted);

						if ((count _KnEnAct) > 0) then
							{
							_ct = 0;
						
								{
								_enX = 0;
								_enY = 0;				

								_VLpos = getPosATL (vehicle (leader _x));
								if (_VLpos in _afront) then 
									{
									_ct = _ct + 1;
									_enX = _enX + (_VLpos select 0);
									_enY = _enY + (_VLpos select 1);
									}
								}
							foreach _KnEnAct;
							
							if (_ct > 0) then
								{
								_enX = _enX/_ct;
								_enY = _enY/_ct;
								};

							_KnEn set [(count _KnEn),[[_enX,_enY,0],_ct]];
							};

						}
					foreach _allied;

					if ((count _KnEn) > 0) then
						{
						_chosenPos = [];	
						_maxTempt = 0;			

							{
							_VHQpos = getPosATL (vehicle (leader _HQ));
							_enPos = _x select 0;
							_dst = _VHQpos distance _enPos;
							_val = _x select 1;
							_actTempt = 0;

							if ((_dst > 0) and ((count _ownForce) > (_val * (0.1 + (random 1)))) and (_val > ((count _alliedForce) * (0.5 + (random 0.5))))) then {_actTempt = (1000 * (sqrt _val))/_dst};

							if (_actTempt > _maxTempt) then
								{
								_maxTempt = _actTempt;
								_chosenPos = _enPos;
								}
							}
						foreach _KnEn;

						if ((count _chosenPos) > 1) then {_chosenPos = [(_chosenPos select 0),(_chosenPos select 1),0]};

						if (_maxTempt > (0.1 + (random 2))) then 
							{
							(group _HQ) setVariable ["inFlank",true];
							[_front,_VHQpos,_chosenPos,2000] call RYD_LocLineTransform;

								{
								_x setPosATL _chosenPos;
								}
							foreach [_o1,_o2,_o3,_o4];

							switch (_HQ) do
								{
								case (leaderHQ) : {RydHQ_NObj = 1};
								case (leaderHQB) : {RydHQB_NObj = 1};
								case (leaderHQC) : {RydHQC_NObj = 1};
								case (leaderHQD) : {RydHQD_NObj = 1};
								case (leaderHQE) : {RydHQE_NObj = 1};
								case (leaderHQF) : {RydHQF_NObj = 1};
								case (leaderHQG) : {RydHQG_NObj = 1};
								case (leaderHQH) : {RydHQH_NObj = 1};
								};

							waitUntil 
								{
								sleep 120;

								_nObj = RydHQ_NObj;
								_reck = RydHQ_Recklessness;
								_cons = RydHQ_Consistency;

								_limit = RydHQ_CaptLimit;

								switch (_HQ) do
									{
									case (leaderHQB) : {_nObj = RydHQB_NObj;_reck = RydHQB_Recklessness;_cons = RydHQB_Consistency;_limit = RydHQB_CaptLimit};
									case (leaderHQC) : {_nObj = RydHQC_NObj;_reck = RydHQC_Recklessness;_cons = RydHQC_Consistency;_limit = RydHQC_CaptLimit};
									case (leaderHQD) : {_nObj = RydHQD_NObj;_reck = RydHQD_Recklessness;_cons = RydHQD_Consistency;_limit = RydHQD_CaptLimit};
									case (leaderHQE) : {_nObj = RydHQE_NObj;_reck = RydHQE_Recklessness;_cons = RydHQE_Consistency;_limit = RydHQE_CaptLimit};
									case (leaderHQF) : {_nObj = RydHQF_NObj;_reck = RydHQF_Recklessness;_cons = RydHQF_Consistency;_limit = RydHQF_CaptLimit};
									case (leaderHQG) : {_nObj = RydHQG_NObj;_reck = RydHQG_Recklessness;_cons = RydHQG_Consistency;_limit = RydHQG_CaptLimit};
									case (leaderHQH) : {_nObj = RydHQH_NObj;_reck = RydHQH_Recklessness;_cons = RydHQH_Consistency;_limit = RydHQH_CaptLimit};
									};

								if (isNil "_limit") then {_limit = 10};

								if (isNull (group _HQ)) then {_nObj = 100};
								if not (alive _HQ) then {_nObj = 100};

								_AllV = _chosenPos nearEntities [["AllVehicles"],300];
								_Civs = _chosenPos nearEntities [["Civilian"],300];
								_AllV2 = _chosenPos nearEntities [["AllVehicles"],500];
								_Civs2 = _chosenPos nearEntities [["Civilian"],500];

								_AllV = _AllV - _Civs;
								_AllV2 = _AllV2 - _Civs2;

								_AllV0 = _AllV;
								_AllV20 = _AllV2;

									{
									if not (_x isKindOf "Man") then
										{
										if ((count (crew _x)) == 0) then {_AllV = _AllV - [_x]}
										}
									}
								foreach _AllV0;

									{
									if not (_x isKindOf "Man") then
										{
										if ((count (crew _x)) == 0) then {_AllV2 = _AllV2 - [_x]}
										}
									}
								foreach _AllV20;

								_NearAllies = _HQ countfriendly _AllV;
								_NearEnemies = _HQ countenemy _AllV2;

								((_nObj > 1) or ((_NearAllies >= _limit) and (_NearEnemies <= ((_reck/(0.5 + _cons))*10))))
								};

							if not (isNull (group _HQ)) then 
								{
								_front setPosition _frPos;
								_front setDirection _frDir;
								_front setSize _frDim; 

									{
									_x setPosATL _actOPos;
									}
								foreach [_o1,_o2,_o3,_o4];

								switch (_HQ) do
									{
									case (leaderHQ) : {RydHQ_NObj = 1};
									case (leaderHQB) : {RydHQB_NObj = 1};
									case (leaderHQC) : {RydHQC_NObj = 1};
									case (leaderHQD) : {RydHQD_NObj = 1};
									case (leaderHQE) : {RydHQE_NObj = 1};
									case (leaderHQF) : {RydHQF_NObj = 1};
									case (leaderHQG) : {RydHQG_NObj = 1};
									case (leaderHQH) : {RydHQH_NObj = 1};
									};

								(group _HQ) setVariable ["inFlank",false]
								};
							}
						}
					}
				};

			(_actO select 2)
			};

		if (isNull (group _HQ)) exitWith {};
		if not (alive _HQ) exitWith {};

		_garrPool = 0;

			{
			_fG = RydHQ_NCrewInfG - (RydHQ_Exhausted + RydHQ_Garrison);

			switch (_x) do
				{
				case (leaderHQB) : {_fG = RydHQB_NCrewInfG - (RydHQB_Exhausted + RydHQB_Garrison)};
				case (leaderHQC) : {_fG = RydHQC_NCrewInfG - (RydHQC_Exhausted + RydHQC_Garrison)};
				case (leaderHQD) : {_fG = RydHQD_NCrewInfG - (RydHQD_Exhausted + RydHQD_Garrison)};
				case (leaderHQE) : {_fG = RydHQE_NCrewInfG - (RydHQE_Exhausted + RydHQE_Garrison)};
				case (leaderHQF) : {_fG = RydHQF_NCrewInfG - (RydHQF_Exhausted + RydHQF_Garrison)};
				case (leaderHQG) : {_fG = RydHQG_NCrewInfG - (RydHQG_Exhausted + RydHQG_Garrison)};
				case (leaderHQH) : {_fG = RydHQH_NCrewInfG - (RydHQH_Exhausted + RydHQH_Garrison)};
				};

			if ((count _fG) > 2) then {_garrPool = _garrPool + 1}
			}
		foreach _reserve;

		if (_garrPool == 0) then
			{
			_fG = RydHQ_NCrewInfG - (RydHQ_Exhausted + RydHQ_Garrison);
			_garrison = RydHQ_Garrison;

			switch (_HQ) do
				{
				case (leaderHQB) : {_fG = RydHQB_NCrewInfG - (RydHQB_Exhausted + RydHQB_Garrison);_garrison = RydHQB_Garrison};
				case (leaderHQC) : {_fG = RydHQC_NCrewInfG - (RydHQC_Exhausted + RydHQC_Garrison);_garrison = RydHQC_Garrison};
				case (leaderHQD) : {_fG = RydHQD_NCrewInfG - (RydHQD_Exhausted + RydHQD_Garrison);_garrison = RydHQD_Garrison};
				case (leaderHQE) : {_fG = RydHQE_NCrewInfG - (RydHQE_Exhausted + RydHQE_Garrison);_garrison = RydHQE_Garrison};
				case (leaderHQF) : {_fG = RydHQF_NCrewInfG - (RydHQF_Exhausted + RydHQF_Garrison);_garrison = RydHQF_Garrison};
				case (leaderHQG) : {_fG = RydHQG_NCrewInfG - (RydHQG_Exhausted + RydHQG_Garrison);_garrison = RydHQG_Garrison};
				case (leaderHQH) : {_fG = RydHQH_NCrewInfG - (RydHQH_Exhausted + RydHQH_Garrison);_garrison = RydHQH_Garrison};
				};

			if (((count _fG)/10) >= 1) then
				{
				_chosen = _fG select 0;

				_dstMin = (_actO select 0) distance (vehicle (leader _chosen));

					{
					_actG = _x;
					_actDst = (_actO select 0) distance (vehicle (leader _actG));
	
					if (_actDst < _dstMin) then 
						{
						_dstMin = _actDst;
						_chosen = _actG
						}
					}
				foreach _fG;

				_chosen setVariable ["Busy" + (str _chosen),true];

				_garrison set [(count _garrison),_chosen]
				}
			};

		switch (_HQ) do
			{
			case (leaderHQ) : {RydHQ_NObj = 5};
			case (leaderHQB) : {RydHQB_NObj = 5};
			case (leaderHQC) : {RydHQC_NObj = 5};
			case (leaderHQD) : {RydHQD_NObj = 5};
			case (leaderHQE) : {RydHQE_NObj = 5};
			case (leaderHQF) : {RydHQF_NObj = 5};
			case (leaderHQG) : {RydHQG_NObj = 5};
			case (leaderHQH) : {RydHQH_NObj = 5};
			};

		switch (_HQ) do
			{
			case (leaderHQ) : {_BBProg = (group leaderHQ) getVariable ["BBProgress",0];(group leaderHQ) setVariable ["BBProgress",_BBProg + 1]};
			case (leaderHQB) : {_BBProg = (group leaderHQB) getVariable ["BBProgress",0];(group leaderHQB) setVariable ["BBProgress",_BBProg + 1]};
			case (leaderHQC) : {_BBProg = (group leaderHQC) getVariable ["BBProgress",0];(group leaderHQC) setVariable ["BBProgress",_BBProg + 1]};
			case (leaderHQD) : {_BBProg = (group leaderHQD) getVariable ["BBProgress",0];(group leaderHQD) setVariable ["BBProgress",_BBProg + 1]};
			case (leaderHQE) : {_BBProg = (group leaderHQE) getVariable ["BBProgress",0];(group leaderHQE) setVariable ["BBProgress",_BBProg + 1]};
			case (leaderHQF) : {_BBProg = (group leaderHQF) getVariable ["BBProgress",0];(group leaderHQF) setVariable ["BBProgress",_BBProg + 1]};
			case (leaderHQG) : {_BBProg = (group leaderHQG) getVariable ["BBProgress",0];(group leaderHQG) setVariable ["BBProgress",_BBProg + 1]};
			case (leaderHQH) : {_BBProg = (group leaderHQH) getVariable ["BBProgress",0];(group leaderHQH) setVariable ["BBProgress",_BBProg + 1]};
			};

		_HandledArray = _HandledArray - [_cSum];
		missionNameSpace setVariable [_varName,_HandledArray];

		if (RydBB_LRelocating) then
			{
			[(group _HQ)] call RYD_WPdel;
			_wp = [(group _HQ),_actOPos,"HOLD","AWARE","GREEN","LIMITED",["true",""],true,50,[0,0,0],"FILE"] call RYD_WPadd
			}
		};

	if (RydBB_Debug) then
		{
			{
			deleteMarker _x
			}
		foreach (_marksT + [_m])
		};

	if not (isNull (group _HQ)) then {(group _HQ) setVariable ["PathDone",true]};
	};

RYD_ReserveExecuting = 
	{
	private ["_HQ","_ahead","_frontPos","_o1","_o2","_o3","_o4","_allied","_HQpos","_front","_angle","_dst","_dstF","_dDst","_stancePos","_taken","_fG","_val","_forGarr","_ct","_ct2",
	"_garrison","_task","_hMany","_busy","_Wpos","_mark","_wp","_aheadL","_aliveHQ","_hostileG","_assg","_possPos","_enV","_posArr","_enV2","_nr","_sX","_sY","_dstA","_amnt","_actT",
	"_maxT","_poss","_m","_side","_rColor"];

	_HQ = _this select 0;
	_ahead = _this select 1;
	_o1 = _this select 2;
	_o2 = _this select 3;
	_o3 = _this select 4;
	_o4 = _this select 5;
	_allied = _this select 6;//leader units

	_HQpos = getPosATL (vehicle _HQ);
	_front = _this select 7;
	_taken = _this select 8;
	_hostileG = _this select 9;
	_side = _this select 10;

	_frontPos = _HQpos;
	if ((count _ahead) > 0) then 
		{
		_aheadL = _ahead select (floor (random (count _ahead)));
		_aliveHQ = true;
		if not (alive _aheadL) then {_aliveHQ = false};
		if (isNull _aheadL) then {_aliveHQ = false};

		if (_aliveHQ) then
			{
			_frontPos = getPosATL (vehicle _aheadL)
			}
		};

	_dst = _HQpos distance _frontPos;

	_dDst = 1000 + (random 1000);

	_dstF = _dst - _dDst;
	if (_dstF < 0) then {_dstF = _dst/2};

	_angle = [_HQpos,_frontPos,10] call RYD_AngTowards;

	if (_angle < 0) then {_angle = _angle + 360};

	_angle = _angle + 180;

	_stancePos = [_frontPos,_angle,_dstF] call RYD_PosTowards2D;
//diag_log format ["1 sp: %1",_stancePos];
	_stancePos = [(_stancePos select 0),(_stancePos select 1),0];
//diag_log format ["2 sp: %1",_stancePos];
	if (surfaceIsWater [(_stancePos select 0),(_stancePos select 1)]) then {_stancePos = _HQpos};

	if ((count _hostileG) == 0) then
		{
			{
			_x setPosATL _stancePos
			}
		foreach [_o1,_o2,_o3,_o4]
		};

	(group _HQ) setVariable ["ObjInit",true];

	[(group _HQ)] call RYD_WPdel;
	_wp = [(group _HQ),_StancePos,"HOLD","AWARE","GREEN","LIMITED",["true",""],true,50,[0,0,0],"FILE"] call RYD_WPadd;

	_fG = RydHQ_NCrewInfG - (RydHQ_Exhausted + RydHQ_Garrison);
	_garrison = RydHQ_Garrison;

	switch (_HQ) do
		{
		case (leaderHQ) : {RydHQ_NObj = 1};
		case (leaderHQB) : {RydHQB_NObj = 1;_fG = RydHQB_NCrewInfG - (RydHQB_Exhausted + RydHQB_Garrison);_garrison = RydHQB_Garrison};
		case (leaderHQC) : {RydHQC_NObj = 1;_fG = RydHQC_NCrewInfG - (RydHQC_Exhausted + RydHQC_Garrison);_garrison = RydHQC_Garrison};
		case (leaderHQD) : {RydHQD_NObj = 1;_fG = RydHQD_NCrewInfG - (RydHQD_Exhausted + RydHQD_Garrison);_garrison = RydHQD_Garrison};
		case (leaderHQE) : {RydHQE_NObj = 1;_fG = RydHQE_NCrewInfG - (RydHQE_Exhausted + RydHQE_Garrison);_garrison = RydHQE_Garrison};
		case (leaderHQF) : {RydHQF_NObj = 1;_fG = RydHQF_NCrewInfG - (RydHQF_Exhausted + RydHQF_Garrison);_garrison = RydHQF_Garrison};
		case (leaderHQG) : {RydHQG_NObj = 1;_fG = RydHQG_NCrewInfG - (RydHQG_Exhausted + RydHQG_Garrison);_garrison = RydHQG_Garrison};
		case (leaderHQH) : {RydHQH_NObj = 1;_fG = RydHQH_NCrewInfG - (RydHQH_Exhausted + RydHQH_Garrison);_garrison = RydHQH_Garrison};
		};

	_fG = _fG - [(group _HQ)];

		{
		if ((count _x) < 5) then {_x set [4,false]};
		if not (_x select 4) then
			{
			_Wpos = _x select 0;
			_val = _x select 1;
			if (_val > 5) then {_val = 5};
			_hMany = floor ((_val/10) * (count _fG));

			//if (_hMany > (ceil (_val/2))) then {_hMany = ceil (_val/2)};
			if (_hMany > 2) then {_hMany = 2};
			//if ((_hMany == 0) and ((random 100) > (90 - (count _fG)))) then {_hMany = 1};

			_ct = 0;

			while {((_ct < _hMany) and ((count _fG) > 0))} do
				{
				_ct = _ct + 1;
				_forGarr = _fG select (floor (random (count _fG)));
				_busy = _forGarr getVariable ("Busy" + (str _forGarr));
				if (isNil "_busy") then {_busy = false};

				_ct2 = 0;

				while {(_busy) and (_ct2 <= (count _fG))} do
					{
					_ct2 = _ct2 + 1;
					_forGarr = _fG select (floor (random (count _fG)));
					_busy = _forGarr getVariable ("Busy" + (str _forGarr));
					if (isNil "_busy") then {_busy = false};
					};

				if not (_busy) then
					{
					_x set [4,true];
					_fG = _fG - [_forGarr];

					[_forGarr,_garrison,_Wpos,_HQ] spawn
						{
						private ["_unitG","_cause","_timer","_alive","_task","_form","_Wpos","_garrison","_HQ","_wp"];

						_unitG = _this select 0;
						_garrison = _this select 1;
						_Wpos = _this select 2;
						_HQ = _this select 3;

						_form = "DIAMOND";
						if (isPlayer (leader _unitG)) then {_form = formation _unitG};
						_unitG setVariable ["Busy" + (str _unitG),true];

						RydHQ_VCDone = false;
						if (isPlayer (leader _unitG)) then {[(leader _unitG),_HQ] spawn VoiceComm;sleep 3;waituntil {sleep 0.1;(RydHQ_VCDone)}} else {if ((random 100) < RydxHQ_AIChatDensity) then {[_UL,RydxHQ_AIC_OrdConf,"OrdConf"] spawn RYD_AIChatter}};

						_task = [(leader _unitG),["Reach the designated position.", "Move", ""],_Wpos] call RYD_AddTask;

						[_unitG] call RYD_WPdel;

						_wp = [_unitG,_Wpos,"MOVE","AWARE","YELLOW","NORMAL",["true","deletewaypoint [(group this), 0]"],true,250,[0,0,0],_form] call RYD_WPadd;

						_cause = [_unitG,6,true,0,30,[],false] call RYD_Wait;
						_timer = _cause select 0;
						_alive = _cause select 1;

						if not (_alive) exitwith {};
						if (_timer > 30) then {[_unitG, (currentWaypoint _unitG)] setWaypointPosition [position (vehicle (leader _unitG)), 1]};

						if ((isPlayer (leader _unitG)) and not (isMultiplayer)) then {(leader _unitG) removeSimpleTask _task};

						if not (_timer > 30) then {_garrison set [(count _garrison),_unitG]};
						}
					}
				}
			}
		}
	foreach _taken;

	if ((count _hostileG) > 0) then
		{
		_assg = [];
		_possPos = [];

			{
			if not (_x in _assg) then
				{
				_enV = vehicle (leader _x);
				_posArr = [];

					{
					_enV2 = vehicle (leader _x);

					if ((_enV distance _enV2) < 600) then 
						{
						_posArr set [(count _posArr),getPosATL _enV2];
						_assg set [(count _assg),_x];			
						}
					}
				foreach _hostileG;

				_nr = count _posArr;

				if (_nr > 0) then
					{
					_sX = 0;
					_sY = 0;

						{
						_sX = _sX + (_x select 0);
						_sY = _sY + (_x select 1);
						}
					foreach _posArr;
					
					_poss = [[_sX/_nr,_sY/_nr,0],_nr];
					if not (surfaceIsWater [_sX/_nr,_sY/_nr]) then {_possPos set [(count _possPos),_poss]}
					}
				}
			}
		foreach _hostileG;

		_stancePos = (_possPos select 0) select 0;
		_maxT = 0;
//diag_log format ["3 sp: %1",_stancePos];
			{
			_dstA = (_x select 0) distance _HQpos;
			_amnt = _x select 1;
			_actT = (_amnt/((_dstA/1000) * (_dstA/1000))) * (0.5 + (random 0.5) + (random 0.5));

			if (_actT > _maxT) then
				{
				_maxT = _actT;
				_stancePos = _x select 0;
				}

			}
		foreach _possPos
		};

		{
		_x setPosATL _stancePos
		}
	foreach [_o1,_o2,_o3,_o4];

	if (RydBB_Debug) then
		{
		_m = (group _HQ) getVariable "ResMark";
		if (isNil "_m") then 
			{
			_rColor = "ColorBlue";
			if (_side == "B") then {_rColor = "ColorRed"};
			_m = [_StancePos,_HQ,"markBBCurrent",_rColor,"ICON","mil_triangle","Reserve area for " + (str _HQ),"",[0.5,0.5]] call RYD_Mark;
			(group _HQ) setVariable ["ResMark",_m]
			}
		else
			{
			_m setMarkerPos _StancePos
			};
		};
	};

RYD_ObjectivesMon = 
	{
	private ["_area","_BBSide","_isTaken","_HQ","_AllV","_Civs","_AllV2","_Civs2","_NearAllies","_NearEnemies","_trg","_AllV0","_AllV20","_mChange","_HQs","_enArea","_enPos","_BBProg"];

	_area = _this select 0;
	_BBSide = _this select 1;
	_HQ = _this select 2;

	while {(RydBB_Active)} do
		{
		sleep 60;

			{
			_isTaken = _x select 2;
			_trg = _x select 0;

			_trg = [_trg select 0,_trg select 1,0];

			if (_isTaken) then
				{
				_AllV = _trg nearEntities [["AllVehicles"],500];
				_Civs = _trg nearEntities [["Civilian"],500];
				_AllV2 = _trg nearEntities [["AllVehicles"],300];
				_Civs2 = _trg nearEntities [["Civilian"],300];

				_AllV = _AllV - _Civs;
				_AllV2 = _AllV2 - _Civs2;

				_AllV0 = _AllV;
				_AllV20 = _AllV2;

					{
					if not (_x isKindOf "Man") then
						{
						if ((count (crew _x)) == 0) then {_AllV = _AllV - [_x]}
						}
					}
				foreach _AllV0;

					{
					if not (_x isKindOf "Man") then
						{
						if ((count (crew _x)) == 0) then {_AllV2 = _AllV2 - [_x]}
						}
					}
				foreach _AllV20;

				_NearAllies = _HQ countfriendly _AllV;
				_NearEnemies = _HQ countenemy _AllV2;

				if (_NearAllies < _NearEnemies) then 
					{
					_x set [2,false];
					_HQs = RydBBa_HQs;
					if (_BBSide == "A") then {RydBBa_Urgent = true} else {RydBBb_Urgent = true;_HQs = RydBBb_HQs};

					_mChange = 10/(count _HQs);

						{
						switch (_x) do
							{
							case (leaderHQ) : {RydHQ_Morale = RydHQ_Morale - _mChange};
							case (leaderHQB) : {RydHQB_Morale = RydHQB_Morale - _mChange};
							case (leaderHQC) : {RydHQC_Morale = RydHQC_Morale - _mChange};
							case (leaderHQD) : {RydHQD_Morale = RydHQD_Morale - _mChange};
							case (leaderHQE) : {RydHQE_Morale = RydHQE_Morale - _mChange};
							case (leaderHQF) : {RydHQF_Morale = RydHQF_Morale - _mChange};
							case (leaderHQG) : {RydHQG_Morale = RydHQG_Morale - _mChange};
							case (leaderHQH) : {RydHQH_Morale = RydHQH_Morale - _mChange};
							};
						}
					foreach _HQs
					}
				}
			else
				{
				_AllV = _trg nearEntities [["AllVehicles"],300];
				_Civs = _trg nearEntities [["Civilian"],300];
				_AllV2 = _trg nearEntities [["AllVehicles"],500];
				_Civs2 = _trg nearEntities [["Civilian"],500];

				_AllV = _AllV - _Civs;
				_AllV2 = _AllV2 - _Civs2;

				_AllV0 = _AllV;
				_AllV20 = _AllV2;

					{
					if not (_x isKindOf "Man") then
						{
						if ((count (crew _x)) == 0) then {_AllV = _AllV - [_x]}
						}
					}
				foreach _AllV0;

					{
					if not (_x isKindOf "Man") then
						{
						if ((count (crew _x)) == 0) then {_AllV2 = _AllV2 - [_x]}
						}
					}
				foreach _AllV20;

				_NearAllies = _HQ countfriendly _AllV;
				_NearEnemies = _HQ countenemy _AllV2;

				if ((_NearAllies >= 10) and (_NearEnemies <= 5)) then 
					{
					_x set [2,true];

					_enArea = missionNameSpace getVariable ["B_SAreas",[]];
					if (_BBSide == "B") then {_enArea = missionNameSpace getVariable ["A_SAreas",[]]};

						{
						_enPos = _x select 0;
						_enPos = [_enPos select 0,_enPos select 1,0];
						if ((_enPos distance _trg) < 50) exitWith
							{
							_x set [2,false]
							}
						}
					foreach _enArea;

					_HQs = RydBBa_HQs;
					if (_BBSide == "A") then {RydBBb_Urgent = true} else {RydBBa_Urgent = true;_HQs = RydBBb_HQs};

					_mChange = 20/(count _HQs);

						{
						switch (_x) do
							{
							case (leaderHQ) : {RydHQ_Morale = RydHQ_Morale + _mChange};//_BBProg = (group leaderHQ) getVariable ["BBProgress",0];(group leaderHQ) setVariable ["BBProgress",_BBProg + 1]};
							case (leaderHQB) : {RydHQB_Morale = RydHQB_Morale + _mChange};//_BBProg = (group leaderHQB) getVariable ["BBProgress",0];(group leaderHQB) setVariable ["BBProgress",_BBProg + 1]};
							case (leaderHQC) : {RydHQC_Morale = RydHQC_Morale + _mChange};//_BBProg = (group leaderHQC) getVariable ["BBProgress",0];(group leaderHQC) setVariable ["BBProgress",_BBProg + 1]};
							case (leaderHQD) : {RydHQD_Morale = RydHQD_Morale + _mChange};//_BBProg = (group leaderHQD) getVariable ["BBProgress",0];(group leaderHQD) setVariable ["BBProgress",_BBProg + 1]};
							case (leaderHQE) : {RydHQE_Morale = RydHQE_Morale + _mChange};//_BBProg = (group leaderHQE) getVariable ["BBProgress",0];(group leaderHQE) setVariable ["BBProgress",_BBProg + 1]};
							case (leaderHQF) : {RydHQF_Morale = RydHQF_Morale + _mChange};//_BBProg = (group leaderHQF) getVariable ["BBProgress",0];(group leaderHQF) setVariable ["BBProgress",_BBProg + 1]};
							case (leaderHQG) : {RydHQG_Morale = RydHQG_Morale + _mChange};//_BBProg = (group leaderHQG) getVariable ["BBProgress",0];(group leaderHQG) setVariable ["BBProgress",_BBProg + 1]};
							case (leaderHQH) : {RydHQH_Morale = RydHQH_Morale + _mChange};//_BBProg = (group leaderHQH) getVariable ["BBProgress",0];(group leaderHQH) setVariable ["BBProgress",_BBProg + 1]};
							};
						}
					foreach _HQs
					}
				}
			}
		foreach _area
		}
	};

RYD_ObjMark = 
	{
	private ["_obj","_mark","_taken","_color","_txt","_AllV","_pos","_side","_alpha"];

	_obj = _this select 0;
	_mark = _this select 1;
	_side = _this select 2;
	
	while {((RydBB_Active) and (RydBB_Debug))} do
		{
		sleep 20;
		_pos = _obj select 0;
		_pos = [_pos select 0,_pos select 1,0];

		_taken = _obj select 2;
		_color = "ColorYellow";
		_alpha = 0.1;

		if ((_taken) and (_side == "A")) then {_color = "ColorBlue";_alpha = 0.5};
		if ((_taken) and (_side == "B")) then {_color = "ColorRed";_alpha = 0.5};
//_AllV = _pos nearEntities [["AllVehicles"],300];
//diag_log format ["obj: %1 col: %2",_obj,_color];
		//_txt = format [" status: %1",count _AllV];

		_mark setMarkerColor _color;

		_mark setMarkerAlpha _alpha;
		//_mark setMarkerText _txt;
		}
	};

RYD_ClusterA = 
	{
	private ["_points","_clusters","_checked","_newCluster","_point","_range","_sum"];

	_points = _this select 0;
	_range = _this select 1;

	_clusters = [];
	_checked = [];
	_newCluster = [];

	//_points2 = +_points;

		{
		_sum = (_x select 0) + (_x select 1);
		if not (_sum in _checked) then
			{
			_checked set [(count _checked),_sum];
			_point = _x;
			_newCluster = [_point];

				{
				_sum = (_x select 0) + (_x select 1);
				if not (_sum in _checked) then
					{
					if ((_point distance _x) <= _range) then 
						{
						_checked set [(count _checked),_sum];
						_newCluster set [(count _newCluster),_x];
						}
					}
				}
			foreach _points;

			_clusters set [(count _clusters),_newCluster]
			}
		}
	foreach _points;

	_clusters
	};

RYD_ClusterB = 
	{
	private ["_points","_clusters","_point","_sumC","_sumS","_sumMin","_pointMin","_dstMin","_sum","_dstAct","_added","_cluster","_inside"];

	_points = _this select 0;

	_clusters = [];

		{
		_point = _x;
		_sumC = (_point select 0) + (_point select 1);

		_added = false;
		_inside = false;

			{
				{
				_sum = (_x select 0) + (_x select 1);
				if (_sum == _sumC) exitWith {_inside = true}
				}
			foreach _x;

			if (_inside) exitWith {}
			}
		foreach _clusters;

		if not (_inside) then
			{
			_sumMin = _sumC;
			_pointMin = _point;
			_dstMin = 100000;

				{
				_sum = (_x select 0) + (_x select 1);

				if not (_sumC == _sum) then
					{
					_dstAct = _point distance _x;
					if (_dstAct < _dstMin) then
						{
						_dstMin = _dstAct;
						_pointMin = _x;
						_sumMin = _sum;
						}
					}
				}
			foreach _points;

				{
				_cluster = _x;

					{
					_sumS = (_x select 0) + (_x select 1);
					if (_sumS == _sumMin) exitWith 
						{
						_added = true;
						_cluster set [(count _cluster),_point]
						};
					}
				foreach _cluster;

				if (_added) exitWith {}
				}
			foreach _clusters;

			if not (_added) then 
				{
				_clusters set [(count _clusters),[_point,_pointMin]];
				};
			}
		}
	foreach _points;

	_clusters
	};

RYD_Cluster = 
	{
	private ["_points","_clusters","_centers","_cluster","_midX","_midY","_center","_clustersC","_newClusters","_newCluster","_clusterNearby","_centerC"];

	_points = _this select 0;

	_clusters = [_points] call RYD_ClusterB;

	_centers = [];


		{
		_cluster = _x;	

		_midX = 0;
		_midY = 0;

			{
			_midX = _midX + (_x select 0);
			_midY = _midY + (_x select 1);
			}
		foreach _cluster;

		_center = [_midX/(count _cluster),_midY/(count _cluster),0];
		_centers set [(count _centers),_center];
		}
	foreach _clusters;

	_clusters set [(count _clusters),_centers];

	_clustersC = [_centers,500] call RYD_ClusterA;

	_newClusters = [];

		{
		_newCluster = [];
		_clusterNearby = [];

			{
			_centerC = _x;

				{
				if (((_centers select _foreachIndex) select 0) == (_centerC select 0)) then {_clusterNearby set [(count _clusterNearby),(_clusters select _foreachIndex)]}
				}
			foreach _clusters
			}
		foreach _x;

			{
				{
				_newCluster set [(count _newCluster),_x]
				}
			foreach _x
			}
		foreach _clusterNearby;

		_newClusters set [(count _newClusters),_newCluster]
		}
	foreach _clustersC;

	_clusters = _newClusters;

	_clusters
	};

RYD_isOnMap = 
	{
	private ["_pos","_onMap","_pX","_pY","_mapMinX","_mapMinY"];

	_pos = _this select 0;
	_onMap = true;

	_pX = _pos select 0;
	_pY = _pos select 1;

	_mapMinX = 0;
	_mapMinY = 0;

	if not (isNil "RydBB_MC") then
		{
		_mapMinX = RydBB_MapXMin;
		_mapMinY = RydBB_MapYMin
		};

	if (_pX < _mapMinX) then 
		{
		_onMap = false
		}
	else
		{
		if (_pY < _mapMinY) then
			{
			_onMap = false
			}
		else
			{
			if (_pX > RydBB_MapXMax) then
				{
				_onMap = false
				}
			else
				{
				if (_pY > RydBB_MapYMax) then
					{
					_onMap = false
					}
				}
			}
		};

	_onMap
	};

RYD_BBSimpleD = 
	{
	private ["_HQs","_BBSide","_clusters","_enPos","_ens","_centers","_center","_amounts","_amount","_midX","_midY","_frs","_frCenters","_frCenter","_lPos","_lng","_angle","_arrow","_colorArr","_mainCenter",
	"_amounts","_amount","_battles","_battle","_angleBatt","_tooClose","_mPos","_mSize","_dstAct","_colorBatt","_sizeBatt","_oldSize","_HQPosMark"];

	_HQs = _this select 0;
	_BBSide = _this select 1;

	sleep 60;

	while {(({not (isNull (group _x))} count _HQs) > 0)} do
		{

		if (({not (isNull (group _x))} count _HQs) == 0) exitWith {};

		_enPos = [];
		_frCenters = [];

			{
			if (alive _x) then
				{
				_ens = RydHQ_KnEnPos;
				_frs = RydHQ_Friends;

				switch (_x) do
					{
					case (leaderHQB) : {_ens = RydHQB_KnEnPos;_frs = RydHQB_Friends};
					case (leaderHQC) : {_ens = RydHQC_KnEnPos;_frs = RydHQC_Friends};
					case (leaderHQD) : {_ens = RydHQD_KnEnPos;_frs = RydHQD_Friends};
					case (leaderHQE) : {_ens = RydHQE_KnEnPos;_frs = RydHQE_Friends};
					case (leaderHQF) : {_ens = RydHQF_KnEnPos;_frs = RydHQF_Friends};
					case (leaderHQG) : {_ens = RydHQG_KnEnPos;_frs = RydHQG_Friends};
					case (leaderHQH) : {_ens = RydHQH_KnEnPos;_frs = RydHQH_Friends};
					};

				_enPos = _enPos + _ens;
				};

			_lPos = (group _x) getVariable "LastCenter";
			_frCenter = _lPos;

			_midX = 0;
			_midY = 0;

				{
				_midX = _midX + ((getPosATL (vehicle (leader _x))) select 0);
				_midY = _midY + ((getPosATL (vehicle (leader _x))) select 1);
				}
			foreach _frs;

			if ((count _frs) > 0) then
				{
				if ([[_midX/(count _frs),_midY/(count _frs)]] call RYD_isOnMap) then 
					{
					_frCenter = [_midX/(count _frs),_midY/(count _frs),0]
					}
				else 
					{
					if (isNil "_lPos") then
						{
						_frCenter = getPosATL (vehicle _x);
						}
					}
				};

			(group _x) setVariable ["LastCenter",_frCenter];

			_frCenters set [(count _frCenters),_frCenter];

			_colorArr = "ColorBlue";
			if (_BBSide == "B") then {_colorArr = "ColorRed"};

			if not (isNil "_lPos") then
				{
				_lng = _lPos distance _frCenter;

				if (_lng > 100) then
					{
					_angle = [_lPos,_frCenter,5] call RYD_AngTowards;

					_arrow = (group _x) getVariable ["ArrowMark",""];

					if (_arrow == "") then 
						{
						_arrow = [_frCenter,(group _x),"markArrow",_colorArr,"ICON","mil_arrow","","",[({alive (leader _x)} count _frs)/10,_lng/500],_angle] call RYD_Mark;
						(group _x) setVariable ["ArrowMark",_arrow];
						}
					else
						{
						_arrow setMarkerPos _frCenter;
						_arrow setMarkerDir _angle;
						_arrow setMarkerSize [({alive (leader _x)} count _frs)/10,_lng/500]
						}
					}
				};

			if not (isNull (group _x)) then
				{
				_HQPosMark = (group _x) getVariable ["HQPosMark",""];
				if (_HQPosMark == "") then
					{
					_HQPosMark = [(getPosATL (vehicle _x)),(group _x),"HQMark",_colorArr,"ICON","mil_box","Position of " + (str _x),"",[0.5,0.5]] call RYD_Mark;
					(group _x) setVariable ["HQPosMark",_HQPosMark]
					}
				else
					{
					_HQPosMark setMarkerPos (getPosATL (vehicle _x));
					}
				}
			else
				{
				deleteMarker ("HQMark" + (str (group _x)))
				}
			}
		foreach _HQs;

		_midX = 0;
		_midY = 0;

			{
			_midX = _midX + (_x select 0);
			_midY = _midY + (_x select 1);
			}
		foreach _frCenters;

		_mainCenter = [_midX/(count _HQs),_midY/(count _HQs),0];

		_clusters = [];

		if ((count _enPos) > 0) then {_clusters = [_enPos] call RYD_Cluster};

		_centers = [];
		_amounts = [];

			{
			_amount = count _x;

			if (_amount > 2) then
				{
				_midX = 0;
				_midY = 0;

					{
					_midX = _midX + (_x select 0);
					_midY = _midY + (_x select 1);
					}
				foreach _x;

				_centers set [(count _centers),[_midX/(count _x),_midY/(count _x),0]];
				_amounts set [(count _amounts),_amount];
				}
			}
		foreach _clusters;

		_battles = missionNamespace getVariable ["Battlemarks",[]];
		_battle = "";


			{
			_center = _x;
			if ([_center] call RYD_isOnMap) then
				{
				_tooClose = false;

					{
					_mPos = getMarkerPos _x;
					_mSize = getMarkerSize _x;
					_mSize = ((_mSize select 0) + (_mSize select 1)) * 100;
					_dstAct = _center distance _mPos;

					if (_mSize > _dstAct) exitWith {_tooClose = true;_battle = _x}
					}
				foreach _battles;

				_colorBatt = "ColorBlue";
				if (_BBSide == "B") then {_colorBatt = "ColorRed"};
				_sizeBatt = (_amounts select _foreachIndex)/6;
				if (_sizeBatt > 5) then {_sizeBatt = 5};

				_angleBatt = [_mainCenter,_x,0] call RYD_AngTowards;

				if not (_tooClose) then
					{
					_battle = [_x,(random 10000),"markBattle",_colorBatt,"ICON","mil_ambush","","",[_sizeBatt,_sizeBatt],_angleBatt - 90] call RYD_Mark;
					_battles set [(count _battles),_battle];
					missionNamespace setVariable ["Battlemarks",_battles];
					}
				else
					{
					_oldSize = getMarkerSize _battle;
					_oldSize = _oldSize select 0;
				
					if (_sizeBatt > _oldSize) then
						{
						_battle setMarkerColor _colorBatt;
						_battle setMarkerSize [_sizeBatt,_sizeBatt];
						_battle setMarkerDir (_angleBatt - 90)
						}
					}
				}
			}
		foreach _centers;

		sleep 300
		}
	};